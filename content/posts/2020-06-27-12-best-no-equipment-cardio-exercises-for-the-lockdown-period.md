---
template: SinglePost
title: 12 Best No-Equipment Cardio Exercises for the Lockdown Period
status: Featured
date: '2020-06-24'
featuredImage: 'https://ucarecdn.com/fcc37a34-4d03-4b7d-8b31-25226f481fa8/'
excerpt: >-
  The temporary closure of gyms and the social distancing rules, due to the
  COVID-19 lockdown, have left people with the only option of converting their
  living rooms into their workout studios. The best way to exercise in such a
  scenario is to incorporate full-body movements that cover all the joints and
  ensure that everything from knees to the shoulders is involved. 
categories:
  - category: Updates
meta:
  description: >-
    The temporary closure of gyms and the social distancing rules, due to the
    COVID-19 lockdown, have left people with the only option of converting their
    living rooms into their workout studios. The best way to exercise in such a
    scenario is to incorporate full-body movements that cover all the joints and
    ensure that everything from knees to the shoulders is involved. 
  title: 12 Best No-Equipment Cardio Exercises for the Lockdown Period
---
The temporary closure of gyms and the social distancing rules, due to the COVID-19 lockdown, have left people with the only option of converting their living rooms into their workout studios. The only issue – there is no equipment. 

The best way to exercise in such a scenario is to incorporate full-body movements that cover all the joints and ensure that everything from knees to the shoulders is involved. The best ways to get that heart rate up is to focus on big moves like push-ups, plank and squats.

Here is a list of 12 exercises that can help burn the fat and keep fit during the lockdown season: 

**1. High Knees**

![](https://ucarecdn.com/609bb4be-4988-4bc4-b6c4-5c0b009536c6/)

Since this exercise involves running in place, it can be done anywhere in a small space.

Stand with legs together and arms on sides. Lift one knee towards the chest and lower it. Repeat with the other knee. Ding this with alternating knees and pumping arms up and down along proves to be a great lower body exercise.

**2. Box Jump**

![](https://ucarecdn.com/d577050c-77b8-4a7a-aaea-258695f5212d/)

Box jumps also are lower body cardio exercises and help in toning the thighs and buttocks. 

Stand straight with a firm back. Keep your feet slightly apart. Stand one step away from the box, get into a quarter squat position, swing the arms and push the feet off the ground to jump on the box. Don’t land with a thud and try to make it lighter. At a time, do 5 sets of 3 reps to avoid straining the nerves.

**3. Toe taps**

This is a low-impact exercise that can be done in front of even a staircase.

Stand in front of the step. Keep one foot on top and quickly switch legs to bring the other foot on top. Continue alternating feet. Start moving left or right while doing these toe taps.

**4. Squat Jumps**

Along with burning loads of calories, this exercise improves blood circulation levels. 

Stand in a squat position with the feet at shoulder-width distance. Stretch hands in line with the chest and parallel to the floor. Tuck in your core muscles and jump as high as possible. Simultaneously lift your hands above your head and land back in a squat. It is advised to do two sets of 10 reps.

**5. Surya Namaskar**

![](https://ucarecdn.com/d5274cdb-49b9-4e73-86a1-fb04672d99d2/)

This cardiovascular exercise that consists of 12 yoga asanas helps stay fit and keep the mind calm.

Performing each pose with precision and speed helps burn more calories. It is advised to start slow with two rounds of each side and gradually increasing the count.

**6. Cat-Cow**

This is said to be one of the best exercises for the well being of the spinal cord.

Start with being in a position where the wrists and knees are on the floor. Inhale slowly, and on the exhale, round the spine and drop the head toward the floor for the cat posture. Inhale again and lift your head, chest, and tailbone towards the ceiling for the cow posture. Perform this for 30 seconds.

**7. Mountain Climbers**

Burn calories, tone your abs and thighs, strengthen your muscles, and improve your circulation levels with this quick and easy cardio exercise.

Lie on the floor in a plank position. The body should be in a straight line from head to toe. Keep the lower back arched, and bend the left knee towards the chest. Hold the pose for a few seconds and return to the first position. Repeat with the right leg. Experts advise two sets of 20 reps.

**8. Dancing** 

![](https://ucarecdn.com/cf44d525-fc45-4509-880b-8705bd993fdd/)

For some fun cardio at home, the best way considered is dancing. It is good for the cardiovascular system and many dance moves also help to build muscle. While putting on a Ranveer Singh or a Varun Dhawan song and mimicking the steps is a good way to have fun while exercising, experts also suggest more structured lessons from the dance exercise videos that are free to watch. 

**9. Butt Kicks**

Butt kicks are basically the opposite of high knees, where heels are lifted up toward the butt.

Stand with the legs together and arms on sides. Bring one heel towards the butt. Lower the foot and repeat with the other heel. Continue alternating the heels for 60 seconds.

**10. Speed skaters**

The exercise consists of sideways movement that mimics a skater moves, hence the name. 

Start in a curtsy lunge, both knees bent and with the right leg diagonally behind. Bend the right arm and straighten the left arm. Push off the left leg, moving the right leg forward. Bring the left leg diagonally behind while switching arms. Continue ‘skating’ left and right.

**11. Inchworm crawl**

![](https://ucarecdn.com/c7878113-de90-4feb-94e6-8eb12e8f1f82/)

The motion of walking your hands and feet forward, in an inchworm crawl, puts the heart and muscles to work.

Start with a plank position. Bend upwards from the hips. Now, slowly walk towards your hands. As soon as the feet touches the hands, put the hands slowly forward into a plank position. Stiffen the core and do one pushup. Repeat this and do it for 5 mins.

**12. Lunge jumps**

This combines standard lunges with jumps to get the heart pumping.

Start in a lunge with both knees bent at 90-degree angles. Pull the shoulders down, and swing the arms back. Quickly swing the arms upward and jump. Switch legs simultaneously and land in a lunge. 

**A few tips to get the best of a cardio workout are:** 

It is essential to start each session with a 5- to 10-minutes of warmup exercises. This relaxes the muscles and increases the blood flow.

![](https://ucarecdn.com/6b61c1b1-ce2c-4e99-a966-cb079d0a3c15/)

Cooling down too is as important as warmup. Rather than abruptly stopping the workout, it is advised to slow down during the 10 minutes.

Have an exercise partner to have a healthy competition and the spirits up. 

Keep an aim for 150 minutes of exercise in a week.
