---
template: SinglePost
title: Overdoing anything is not good; including exercise
status: Featured
date: '2020-06-27'
featuredImage: 'https://ucarecdn.com/b694d44d-56f4-4efa-a2b9-6ac374d46656/'
excerpt: >-
  While being in lockdown seems a good time to get fit, and it is essential to
  exercise every day, but everyday should not be hard. No pain, no gain is not
  always good and over-exercising can lead to injuries
categories:
  - category: Updates
meta:
  description: >-
    While being in lockdown seems a good time to get fit, and it is essential to
    exercise every day, but everyday should not be hard. No pain, no gain is not
    always good and over-exercising can lead to injuries
  title: Overdoing anything is not good; including exercise
---
**While being in lockdown seems a good time to get fit, and it is essential to exercise every day, but everyday should not be hard. No pain, no gain is not always good and over-exercising can lead to injuries**

![](https://ucarecdn.com/ab1eb026-6222-4885-abf8-32f8891cedee/)

As the lockdown in India started in March, many people have found a lot of free time available to them and plan to utilize this time to get fit. Be it running or joining online classes, getting exercise is not only good for physical health but also has many benefits for mental health too.

Yes, exercise has many benefits, like increasing energy levels, burning calories, and even helps in boosting one’s mood. But too much exercise can leave the person drained and even at the risk of injury. The childhood story of Goldilocks and her principal of "not too big, not too small, but just right" works for exercising too. 

It is essential to understand that everyone’s body is different. Not all workout routines can prove beneficial to all. Also, every person has their own limits on how much their bodies can take. After a tough workout, it is normal to feel some fatigue, but pain, burnout and complete exhaustion are not. If the body becomes sore due to exercise and the soreness stays for more than two days, then it is time to slow down. Athletes and people in high-intensity sports also face something called the ‘Overtraining syndrome.’ It not just affects the body, but can also have serious effects on mental health and can make a person short-tempered, hostile, anxious, and even depressed.

Before one starts to exercise, it is important to know that exercising causes muscle fibers to break. This is usually a good thing, as the body repairs and rebuilds them only to become stronger. But for the repair process to happen, the body needs adequate rest, sleep and nutrition. 

Exercising during the lockdown should not feel like torture. Experts believe that exercise does not have to be in big sessions, but it's about doing the best one can incorporate some rather than none. Choosing to workout over professional obligations and time for family on a consistent basis are signs that the person is has become hyper-focused on exercise that may result in overdoing it. 

![](https://ucarecdn.com/9e5d5ef6-bd1b-4a39-9070-f58ab831c4e1/)

Over-exercising in men can lead to hormonal imbalance, including changes in testosterone and cortisol (stress hormone) levels. This can drive overeating and can also result in the body burning muscle instead of fat. Women too can be affected by over-training. If combined with an eating disorder, in extreme cases, women can even miss their period due to insufficient energy.

![](https://ucarecdn.com/dc828567-1b7a-4380-b091-70cf53c5766d/)

The rule of thumb to follow for exercising is doing something every day, but everyday should not be hard. Experts believe that 'no pain, no gain' is not always good and pushing too hard can lead to injuries.

The correct exercises for beginners include focusing on daily mobility practice to ensure that all the joints are exercised. This can be done through yoga, aerobics or even a daily dance. Strength exercise like lifting weights or even body weights should be done three-four times a week and HIIT (High-Intensity Interval Training) must be for not more than 20 minutes once or twice a week. It is also okay to spend a couple of days in a week doing nothing. The body needs that resting time.
