---
template: SinglePost
title: Traditional Cycle V/S Exercise Bike – An India Perspective
status: Featured
date: '2020-10-12'
featuredImage: 'https://ucarecdn.com/421d6844-943b-4bf5-9e49-c49df1341d44/'
excerpt: >-
  There has been a 72 per cent increase in pollution from 1998 to 2016 in the
  region which accounts for 40 per cent of India’s population. Indoor cycling
  allows you to increase your heart rate, work on your muscles and stay fit &
  healthy without exposing yourself to any pollution. With the number of cars
  and scooters increasing on roads, the cycle paths have become non-existent.
  Indoor bikes with the big screen that Synq.Fit has, provide the same
  experience with complete safety. 
categories:
  - category: Updates
meta:
  description: >-
    There has been a 72 per cent increase in pollution from 1998 to 2016 in the
    region which accounts for 40 per cent of India’s population. Indoor cycling
    allows you to increase your heart rate, work on your muscles and stay fit &
    healthy without exposing yourself to any pollution. With the number of cars
    and scooters increasing on roads, the cycle paths have become non-existent.
    Indoor bikes with the big screen that Synq.Fit has, provide the same
    experience with complete safety. 
  title: Traditional Cycle V/S Exercise Bike – An India Perspective
---
Before getting on to an exercise bike, there is always a thought that comes to people’s mind – Shouldn’t I just buy a regular cycle and go on trips on the highways or the mountains. While this sounds quite adventurous and fun, but here’s how it is not practical in India:

**Pollution**

According to a [report](https://www.hindustantimes.com/india-news/people-in-north-india-may-lose-7-years-of-life-due-to-air-pollution-study/story-JTxm9Jhy6VEqYumz1h2cUJ.html) published in the Hindustan Times, that citizens in the IGP region that includes Delhi, Punjab, Haryana, Uttar Pradesh, West Bengal and Bihar are exposed to greater levels of air pollution that has led to decrease in life span.

This report was based on the Air Quality Life Index (AQLI), produced by the Energy Policy Institute at the University of Chicago (EPIC). It shows there has been a 72 per cent increase in pollution from 1998 to 2016 in the region which accounts for 40 per cent of India’s population.

This issue is something that makes exercising outside really difficult, also the fumes that enter the body due to heavy breathing don’t prove to be helpful. Is going out cycling around the city really beneficial?

Indoor cycling allows you to increase your heart rate, work on your muscles and stay fit & healthy without exposing yourself to any pollution.

![](https://ucarecdn.com/121c5e71-4c30-412d-8328-abb23fef6f02/)

 

**Traffic and Safety**

The number of cyclists killed on roads has increased by 37.7% to 3,559. According to an [article](https://www.thehindu.com/news/national/indian-roads-claim-more-cyclists-in-2017/article25172709.ece#:~:text=However%2C%20the%20number%20of%20pedestrians,in%20road%20accidents%20during%202017.) published in The Hindu, pedestrians and cyclists together accounted for 16.2% of the total fatalities in road accidents in 2017.

With the number of cars and scooters increasing on roads, the cycle paths have become non-existent. Even if the lines are drawn, there are hardly any motor-vehicle drives that respect them. For cyclists, the risks have only increased. While some may argue that the risk isn’t there when you are cycling in the outskirts, but whom are we kidding, the way to outskirts goes through the cities.

Indoor bikes with the big screen that Synq.Fit has, provide the same experience with complete safety. After all, your home is the place where you feel the most comfortable and safe.
