---
template: SinglePost
title: The COVID-19 Pandemic Has Made Fitness Important More Than Ever
status: Featured
date: '2020-09-01'
featuredImage: 'https://ucarecdn.com/c0c1da4f-9875-40b8-b066-c6cffff4a8ea/'
excerpt: >
  Work from home has introduced to new and unique challenges to maintaining
  proper health. It is essential to make fitness a priority during this time of
  crisis – to not just stay fit, but also calm and stress free
categories:
  - category: Updates
meta:
  description: >
    Work from home has introduced to new and unique challenges to maintaining
    proper health. It is essential to make fitness a priority during this time
    of crisis – to not just stay fit, but also calm and stress free
  title: The COVID-19 Pandemic Has Made Fitness Important More Than Ever
---
The COVID-19 pandemic has completely upturned the work culture in India. Majority of the office-going population is now working from home and has resulted in increased lethargy and also possibilities of diseases that come along with it. 

![](https://ucarecdn.com/6394aeee-902c-43c2-9475-5513b4eaab86/)

According to a survey of 165 working professionals of different age groups, work was more “stressful and lethargic” than working from office for almost 90% of the people. This survey by Your Amigos Foundation, a Kochi-based NGO, suggested that almost 50% of the respondents were experiencing work-from-home for the first time in their careers.

This period of quarantine/isolation brings in mixed emotions which can range from relief and gratitude to anger, concern and frustration. While it is normal to feel anxious, but extreme stress, trouble sleeping, and inability to carry out daily routines can require professional help. To avoid such a scenario the American Psychological Association suggests including regular daily activities, such as work and exercise and also integrating other healthy pastimes.

With gyms being temporarily closed, and maintaining social distancing is a priority, many are finding it difficult to adjust to the new normal, which can continue in the post-pandemic period. 

In this period of isolation, it is important to maintain fitness and be physically active. Sitting on a place has become the new smoking that can have a negative impact on a person’s health as it causes weight gain, back pain, and much more. Exercise offers many important benefits and must always be a part of one’s weekly routine. Daily physical activity helps in reducing stress and can help people sleep better. Fitness not just helps in keeping healthy, but can also help in reducing the risk of depression. There are multiple exercises that can be done at home including cardio exercises and weight training.

![](https://ucarecdn.com/5af077c9-6983-4647-b93c-b82f0b13b188/)

Experts believe some time and the motivation for cardio can help a person extensively. One only needs a small space in his/her own house to get a run. From running in place to running and walking inside the house can help a lot. Experts suggest keeping the workout going from a set amount of time and gradually increasing gives similar results to what one would get when exercising on a treadmill or an outdoor course. It might feel funny to pace back and forth across the house, but limited exercise does not mean that the body will not reap its benefits. A 20-30 minutes walk, run, and jog is a good way to start. Combining this with a few minutes of stretching the body every hour can help a person stay fit and healthy. 

In fact, a great advantage of being at home is that one doesn’t need to pack workout clothes, shoes and toiletries anymore. They also save the time of going to and coming back from the gym or place where they exercise. Exercises like burpees and jumping jacks that can be done at one place can help in increasing the heart rate and provide the adrenaline rush that one feels after a heavy exercise. 

Working from home can present some exceptional challenges and especially the ones related to health. While running and jogging on the place can sound simple, but requires a lot of motivation to do. It is now more than ever essential to include fitness in the daily schedule as the abundance of snacking cannot be ignored that comes along with working from home.
