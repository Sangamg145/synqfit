---
template: ContactPage
slug: contact
title: Synq.fit Support Center and Headquaters
featuredImage: 'https://ucarecdn.com/981ce3b7-6fe8-43e4-bd82-6d3abde891c3/'
subtitle: Better Every Day
address: |-
  Synq.fit
  9th Floor, Tower B, SAS Towers, 
  Medicity, Sector 38, Gurugram,
  Haryana – 122 003
phone: '9990406244'
email: ravi@synq.fit
locations:
  - lat: ''
    lng: ''
    mapLink: >-
      https://www.google.com/maps/place/Synq.Work+-+Managed+office+spaces/@28.4411942,77.0394601,15z/data=!4m2!3m1!1s0x0:0x45d021f5813ebc9a?sa=X&ved=2ahUKEwjzt9O6mrnsAhUnwjgGHZB0BDAQ_BIwHnoECBkQBQ
meta:
  description: 'Contact us for the synq.fit Bike '
  title: 'Contact Page | Synq.fit '
---
# Headquater
