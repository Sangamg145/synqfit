import React from 'react'
import { graphql } from 'gatsby'

import PageHeader from '../components/PageHeader'
import Content from '../components/Content'
import Layout from '../components/Layout'
import styles from "./about.module.css"

//here over
import { Link } from 'gatsby'

const About = () => (
  // <Layout meta={page.frontmatter.meta || false}>
   
   <Layout>
    <div className={styles.container}>
       <h1 className={styles.main_head}>About Us</h1>
       <p className={styles.mini_head}>Last Updated: July 20, 2020</p>
    </div>
    <div className={styles.content_container}>

<p className={styles.content}>  One Stop Solution for your workout from home<br/>
It is a digital world and fitness is not behind. In this connected era, it has become essential to combine physical and digital worlds, and Synq.Fit merges these seamlessly for you. We are a new age connected fitness company that keeps you engaged in instructor-led fitness activities. Keep laziness and stress at a bay and leave it to Synq.Fit to solve all your fitness needs with our unique curated program.<br/>
Synq.Fit helps you to maintain these fitness levels by bringing in best in class workout programs at your home. With live sessions every day, Synq.Fit coverts your living room into an energetic fitness studio motivating you to sweat it out. It is essential to maintain a basic fitness level to improve work efficiency and productivity, and Synq.Fit is an ultimate solution to all your cardio issues. <br/>
Bringing in world class technology, Synq.Fit helps you achieve your fitness goals from the comfort of your living room. The membership plans are especially curated to serve all your needs. <br/>
In the last few years, work from home has gained momentum in India. The purpose of this shift was to keep individuals motivated and improve their work life balance, but this nature of work also blurred the lines between personal and professional lives. While work from home is considered a good option, it can lead to many fitness issues in individuals. Lack of physical activity can lead to obesity, type 2 diabetes, heart disease and even cancer.<br/>
Synq.Fit also improves your work life balance by managing your social as well as physical fitness appetite. The program not only adds fun into fitness, it also helps you maintain a good social circle by interacting with friends and family.<br/>
Synq.fit is not just home cardio solution, but a fitness movement. Our users not just stay healthy and fit, but also become a part of the bigger riding movement that aims to make India healthier. <br/></p>
</div>

    </Layout>
 



  // </Layout>
)

export default About

