import React from 'react'
import { graphql } from 'gatsby'

import PageHeader from '../components/PageHeader'
import Content from '../components/Content'
import Layout from '../components/Layout'
import "../pages/product.css"

import logo from "../assets/images/logo.png"

import burg from "../assets/images/burg.png"
import insta from "../assets/images/instagram.png"
import fb from "../assets/images/facebook.png"
import tweet from "../assets/images/twitter.png"
import apple from "../assets/images/apple_store.png"
import android from "../assets/images/android_store.png"


 
// mobile Content
import mob_logo from "../assets/mob_images/logo-compact.png"
import mob_burg from "../assets/mob_images/mobile_burg.png"


//product
import cycle_priority from "../assets/images/product/bike-product-copy.png"
import thirtyday from "../assets/images/product/ic-30-day.png"
import emi from "../assets/images/product/ic-emi.png"
import warranty from "../assets/images/product/ic-warranty.png"

//mobile

//product
import thirtyday_mobile from "../assets/mob_images/product/ic-30-day-mobile.png"
import emi_mobile from "../assets/mob_images/product/ic-emi-mobile.png"
import warranty_mobile from "../assets/mob_images/product/ic-warranty-mobile.png"
import Img from "gatsby-image"
//here over
import { Link } from 'gatsby'

// Export Template for use in CMS preview
export const ProductTemplate = ({ title, subtitle, featuredImage, body }) => (
  <main className="Home">
    <PageHeader
      large
      title={title}
      subtitle={subtitle}
      backgroundImage={featuredImage}
    />

    <section className="section">
      <div className="container">
        <Content source={body} />
      </div>
    </section>
  </main>
)

// Export Default HomePage for front-end
const Product = ({data}) => (
  // <Layout meta={page.frontmatter.meta || false}>
   
   <Layout>
   {console.log(data)}
   
    <div className=" container_priority">
        <div className="prior_img">
             <img src={cycle_priority} style={{width:"1440px"}}></img>
             {/* <Img fluid={data.file.childImageSharp.fluid}
              /> */}

        </div>
        <div>
            <p className="priority_text">Get Priority Access!</p>
            <p className="priority_minitext">Pre-order now and reserve your Synq.fit bike, limited units available. Get 3 months free membership and a limited Synq.fit kit*.</p>
            <a className="priority_button" href="https://rzp.io/l/synqbikefit" target="_blank">Place Order</a>

        </div>


    </div>


{/* mobile  */}
<div className=" container_priority_mobile">
       
       
            <p className="priority_text">Get Priority Access!</p>
            <p className="priority_minitext">Pre-order now and reserve your Synq.fit bike, limited units available. Get 3 months free membership and a limited Synq.fit kit*.</p>
            <a className="priority_button" href="https://rzp.io/l/synqbikefit" target="_blank">Place Order</a>

       


    </div>
{/* over */}


{/* <div className="synq_fitcontainer">
    <p className="kit_head">
        Your synq-fit includes
    </p>
    <div className="synq_kit">
    <div>
        <img className="kit_pc" src={emi}/>
        <img className="kit_mob" src={emi_mobile}/>
        <p className="kit_para">No Cost EMI</p>
    </div>
    <div>
    <img className="kit_pc" src={thirtyday}/>
    <img className="kit_mob" src={thirtyday_mobile}/>


    <p className="kit_para">30-day Home Trial</p>

    </div>
    <div>
    <img className="kit_pc" src={warranty}/>
    <img className="kit_mob" src={warranty_mobile}/>

    <p className="kit_para">12-month Limited<br/> Warranty</p>

    </div>

    </div>
</div> */}

    </Layout>
 



  // </Layout>
)

export default Product



export const pageQuery = graphql`
query product_query {
  __typename
  file(relativePath: {eq: "images/product/bike-product-copy.png"}) {
    childImageSharp {
      fluid{
        aspectRatio
        base64
        srcWebp
        src
      }
    }
  }
 
}
`