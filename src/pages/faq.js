import React,{useState} from 'react'
import { graphql } from 'gatsby'
import PageHeader from '../components/PageHeader'
import Content from '../components/Content'
import Layout from '../components/Layout'
import styles from "../pages/faq.module.css"

import on from "../assets/images/faq/on.png"
import off from "../assets/images/faq/off.png"

//here over
import { Link } from 'gatsby'

const Faq = () =>
{

    const [active,setActive]=useState({
        1:false,
        2:false,
        3:false,
        4:false,
        5:false,
        6:false,
      

    })
    const show =(event)=>
    {
       
        console.log(event.target.value)
        const val = event.target.value
        // console.log(active.event.target.value)
    //    const newstate =Object.keys(active).filter((data)=>data==val?(data:!active[data]):(data:active[data]))
     setActive({...active,[val]:!active[val]})
   
    }
return(
  // <Layout meta={page.frontmatter.meta || false}>
   
   <Layout>
   
   <div className={styles.container}>
       <h1 className={styles.main_head}>FAQs</h1>
       <p className={styles.mini_head}>Frequently Asked Questions</p>
       <div className={styles.flex_container}>
       
        <div className={styles.main}>
            <div className={styles.left_container}>
            <p className={styles.heading}>P<span style={{borderBottom:'5px solid #d6177d'}}>AYMEN</span>T</p>
            <hr/>
            <ul className={styles.list}>
                <li value="1" onClick={show} className={styles.title} >
                <span>{active[1]&&<img src={on}/>}{!active[1]&&<img src={off}/>}</span>Are there any extra fees that i will have to pay?
                <p className={`${!active[1]&&styles.hidden} ${styles.para}`}>Are there any extra fees that i will have to pay any extra fees that i will have to</p>
                </li>
                <li value="2" onClick={show} className={styles.title}>
                <span>{active[2]&&<img src={on}/>}{!active[2]&&<img src={off}/>}</span>Are there any extra fees that i will have to pay?
                <p className={`${!active[2]&&styles.hidden} ${styles.para}`}>Are there any extra fees that i will have to pay any extra fees that i will have to</p>

                </li>
                <li value="3" onClick={show} className={styles.title}>
                <span>{active[3]&&<img src={on}/>}{!active[3]&&<img src={off}/>}</span>Are there any extra fees that i will have to pay?
                <p className={`${!active[3]&&styles.hidden} ${styles.para}`}>Are there any extra fees that i will have to pay any extra fees that i will have to</p>

                </li>
            </ul>

            </div>

            <div className={styles.left_container} >
            <p className={styles.heading}>P<span style={{borderBottom:'5px solid #d6177d'}}>AYMEN</span>T</p>
            <hr/>
            <ul className={styles.list}>
            <li value="4" onClick={show} className={styles.title}>
                <span>{active[4]&&<img src={on}/>}{!active[4]&&<img src={off}/>}</span>Are there any extra fees that i will have to pay?
                <p className={`${!active[4]&&styles.hidden} ${styles.para}`}>Are there any extra fees that i will have to pay any extra fees that i will have to</p>

                </li>
                <li value="5" onClick={show} className={styles.title}>
                <span>{active[5]&&<img src={on}/>}{!active[5]&&<img src={off}/>}</span>Are there any extra fees that i will have to pay?
                <p className={`${!active[5]&&styles.hidden} ${styles.para}`}>Are there any extra fees that i will have to pay any extra fees that i will have to</p>

                </li>
                <li value="6" onClick={show} className={styles.title}>
                <span>{active[6]&&<img src={on}/>}{!active[6]&&<img src={off}/>}</span>Are there any extra fees that i will have to pay?
                <p className={`${!active[6]&&styles.hidden} ${styles.para}`}>Are there any extra fees that i will have to pay any extra fees that i will have to</p>

                </li>
            </ul>

            </div>
        </div>
        
        <div className={styles.main}>

            <div className={styles.right}>
                <p className={styles.right_head}>Not found your question?</p>
                <p className={styles.right_para}>Fill in the form below</p>
                <input className={styles.inputs} placeholder="Name"/>
                <input className={styles.inputs} placeholder="Email"/>
                <textarea rows="5" className={styles.inputs} placeholder="Query"/>
                <button className={styles.btn}>SEND QUESTIONS</button>

            </div>

        </div>
       </div>
   </div>

    </Layout>
 



  // </Layout>
)
}
export default Faq

