import React from 'react'
import Layout from '../components/Layout'
import './thankyou.css' 

function Thankyou(){
    return( 
        <Layout>
            <div className="thank-container">
                <h1>Thank you.</h1>
                <p>Someone from our team will get back to you soon.</p>
            </div>
        </Layout> 
        
    )
}

export default Thankyou