import React,{useState} from 'react'
import { graphql } from 'gatsby'
import PageHeader from '../components/PageHeader'
import Content from '../components/Content'
import Layout from '../components/Layout'
import styles from "../pages/policy.module.css"


//here over
import { Link } from 'gatsby'

const Policy = () =>
{

return(
  // <Layout meta={page.frontmatter.meta || false}>
   
   <Layout>
   
   <div className={styles.container}>
       <h1 className={styles.main_head}>Privacy Policy</h1>
       <p className={styles.mini_head}>Last Updated: July 20, 2020</p>
    </div>

    <div className={styles.content_container}>
    <p className={styles.content}>This page informs you of our policies regarding the collection, use and disclosure of Personal Information when you use our Service.</p>

    <p className={styles.content}>We will not use or share your information with anyone except as described in this Privacy Policy.
    </p>
    <p className={styles.content}>We use your Personal Information for providing and improving the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, accessible at https://www.abc.com.
</p>
<p className={styles.content_head}>Information Collection And Use</p>
<p className={styles.content}>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to, your email address, name, phone number, postal address, other information (“Personal Information”).
</p>
<p className={styles.content}>We collect this information for the purpose of providing the Service, identifying and communicating with you, responding to your requests/inquiries, servicing your purchase orders, and improving our services.
</p>
<p className={styles.content_head}>Log Data</p>
<p className={styles.content}>We collect information that your browser sends whenever you visit our Service (“Log Data”). This Log Data may include information such as your computer’s Internet Protocol (“IP”) address, browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages and other statistics.
</p>
<p className={styles.content}>In addition, we may use third party services such as Google Analytics that collect, monitor and analyze this type of information in order to increase our Service’s functionality. These third party service providers have their own privacy policies addressing how they use such information.
</p>
<p className={styles.content_head}>Cookies</p>
<p className={styles.content}>What Are Cookies?</p>

<p className={styles.content}>As is common practice with almost all professional websites this site uses cookies, which are tiny files that are downloaded to your computer, to improve your experience. This page describes what information they gather, how we use it and why we sometimes need to store these cookies. We will also share how you can prevent these cookies from being stored however this may downgrade or ‘break’ certain elements of the sites functionality.
</p>
<p className={styles.content}>For more general information on cookies see the Wikipedia article on HTTP Cookies.
</p>
<p className={styles.content_head}>How We Use Cookies</p>
<p className={styles.content}>We use cookies for a variety of reasons detailed below. Unfortunately in most cases there are no industry standard options for disabling cookies without completely disabling the functionality and features they add to this site. It is recommended that you leave on all cookies if you are not sure whether you need them or not in case they are used to provide a service that you use.
</p>           
   </div>

    </Layout>
 



  // </Layout>
)
}
export default Policy

