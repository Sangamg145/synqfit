import React,{useState} from 'react'
import { graphql } from 'gatsby'

import PageHeader from '../components/PageHeader'
import Content from '../components/Content'

import Spin from '../components/Spinner'

import Layout from '../components/Layout'
import styles from "../pages/team.module.css"

import dummy from "../assets/images/img-2.png"
import Arrow from "../assets/arrow.png"
import Modal from 'react-modal';
//here over
import { Link } from 'gatsby'

const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)'
    }
  };
   
const Team = () => {
    
    const [modalIsOpen,setIsOpen] = React.useState({
        // 1:false,
        // 2:false,
        // 3:false,
        // 4:false,
        // 5:false,
        // 6:false,
        // 7:false,
        // 8:false,
    
    });

    function openModal(e) {
        console.log(e.target.value)
        const id=e.target.dataset.id
        // console.log(modalIsOpen[e.target.dataset.id])
    //   setIsOpen({[id]:true});
    }
   
  
    function afterOpenModal() {
      // references are now sync'd and can be accessed.
    }
   
    function closeModal(e){
        setIsOpen(false)
    //   setIsOpen(...modalIsOpen,{[e.target.value]:false});
    }
   
  
    

    return(
  // <Layout meta={page.frontmatter.meta || false}>
   
   <Layout>
        <Modal
          isOpen={modalIsOpen[1]}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Trainer Information"
        >
 
         <div className={styles.modalcontainer}>
            <div>
                <img src={dummy}/>
                <p className={styles.name}>Gagan Deep Khajotia</p>
                <p className={styles.certi}>Certifications: ACE Certified, AFFA Certified, Milon certified,SPPINNG Certified</p>
                <p className={styles.exp}>Experience: 10 years</p>
            </div>
            <div>
             <p className={styles.bio}>Biography</p>
             <p className={styles.biodata}>Gagan is an artist! With a passion for fitness and music, he blends the two in a perfect way for an enchanted experience. An expert in Cycling, CrossFit and Strength & Conditioning training, Gagan is currently the best instructor in India who is also a DJ! With proficiency in conducting group classes, Gagan can provide the greatest cycling experience over music. Using his music skills, he ensures that the riders are constantly engaged and takes them on a journey.
            Gagan truly believes that online sessions are the 'Future of Fitness'. As people get busy with their schedules and are left with little or no time in hand to visit the gym, going digital will soon become the most preferred way to keep fit.
            Along with fitness, music and Djing, Gagan also finds interest in singing and dancing. Making the session fun, energizing and interesting, Gagan is here to 'Fuel your muscles with Music.'
            If you are looking for a HIIT musical session with dance, Gagan will ensure that you are all sweaty and pumped up.</p>
</div>
         </div>
        </Modal>


       


        <Modal
          isOpen={modalIsOpen[2]}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Trainer Information"
        >
 
         <div className={styles.modalcontainer}>
            <div>
                <img src={dummy}/>
                <p className={styles.name}>Dummy Thing</p>
                <p className={styles.certi}>Certifications: ACE Certified, AFFA Certified, Milon certified,SPPINNG Certified</p>
                <p className={styles.exp}>Experience: 10 years</p>
            </div>
            <div>
             <p className={styles.bio}>Biography</p>
             <p className={styles.biodata}>Dummy is an artist! With a passion for fitness and music, he blends the two in a perfect way for an enchanted experience. An expert in Cycling, CrossFit and Strength & Conditioning training, Gagan is currently the best instructor in India who is also a DJ! With proficiency in conducting group classes, Gagan can provide the greatest cycling experience over music. Using his music skills, he ensures that the riders are constantly engaged and takes them on a journey.
            Gagan truly believes that online sessions are the 'Future of Fitness'. As people get busy with their schedules and are left with little or no time in hand to visit the gym, going digital will soon become the most preferred way to keep fit.
            Along with fitness, music and Djing, Gagan also finds interest in singing and dancing. Making the session fun, energizing and interesting, Gagan is here to 'Fuel your muscles with Music.'
            If you are looking for a HIIT musical session with dance, Gagan will ensure that you are all sweaty and pumped up.</p>
</div>
         </div>
        </Modal>



        <Modal
          isOpen={modalIsOpen[3]}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Trainer Information"
        >
 
         <div className={styles.modalcontainer}>
            <div>
                <img src={dummy}/>
                <p className={styles.name}>Test Case</p>
                <p className={styles.certi}>Certifications: ACE Certified, AFFA Certified, Milon certified,SPPINNG Certified</p>
                <p className={styles.exp}>Experience: 10 years</p>
            </div>
            <div>
             <p className={styles.bio}>Biography</p>
             <p className={styles.biodata}>Gagan is an artist! With a passion for fitness and music, he blends the two in a perfect way for an enchanted experience. An expert in Cycling, CrossFit and Strength & Conditioning training, Gagan is currently the best instructor in India who is also a DJ! With proficiency in conducting group classes, Gagan can provide the greatest cycling experience over music. Using his music skills, he ensures that the riders are constantly engaged and takes them on a journey.
            Gagan truly believes that online sessions are the 'Future of Fitness'. As people get busy with their schedules and are left with little or no time in hand to visit the gym, going digital will soon become the most preferred way to keep fit.
            Along with fitness, music and Djing, Gagan also finds interest in singing and dancing. Making the session fun, energizing and interesting, Gagan is here to 'Fuel your muscles with Music.'
            If you are looking for a HIIT musical session with dance, Gagan will ensure that you are all sweaty and pumped up.</p>
</div>
         </div>
        </Modal>


        <Modal
          isOpen={modalIsOpen[4]}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Trainer Information"
        >
 
         <div className={styles.modalcontainer}>
            <div>
                <img src={dummy}/>
                <p className={styles.name}>Another Dummy</p>
                <p className={styles.certi}>Certifications: ACE Certified, AFFA Certified, Milon certified,SPPINNG Certified</p>
                <p className={styles.exp}>Experience: 10 years</p>
            </div>
            <div>
             <p className={styles.bio}>Biography</p>
             <p className={styles.biodata}>Gagan is an artist! With a passion for fitness and music, he blends the two in a perfect way for an enchanted experience. An expert in Cycling, CrossFit and Strength & Conditioning training, Gagan is currently the best instructor in India who is also a DJ! With proficiency in conducting group classes, Gagan can provide the greatest cycling experience over music. Using his music skills, he ensures that the riders are constantly engaged and takes them on a journey.
            Gagan truly believes that online sessions are the 'Future of Fitness'. As people get busy with their schedules and are left with little or no time in hand to visit the gym, going digital will soon become the most preferred way to keep fit.
            Along with fitness, music and Djing, Gagan also finds interest in singing and dancing. Making the session fun, energizing and interesting, Gagan is here to 'Fuel your muscles with Music.'
            If you are looking for a HIIT musical session with dance, Gagan will ensure that you are all sweaty and pumped up.</p>
</div>
         </div>
        </Modal>


        <Modal
          isOpen={modalIsOpen[5]}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Trainer Information"
        >
 
         <div className={styles.modalcontainer}>
            <div>
                <img src={dummy}/>
                <p className={styles.name}>Second Dummy</p>
                <p className={styles.certi}>Certifications: ACE Certified, AFFA Certified, Milon certified,SPPINNG Certified</p>
                <p className={styles.exp}>Experience: 10 years</p>
            </div>
            <div>
             <p className={styles.bio}>Biography</p>
             <p className={styles.biodata}>Gagan is an artist! With a passion for fitness and music, he blends the two in a perfect way for an enchanted experience. An expert in Cycling, CrossFit and Strength & Conditioning training, Gagan is currently the best instructor in India who is also a DJ! With proficiency in conducting group classes, Gagan can provide the greatest cycling experience over music. Using his music skills, he ensures that the riders are constantly engaged and takes them on a journey.
            Gagan truly believes that online sessions are the 'Future of Fitness'. As people get busy with their schedules and are left with little or no time in hand to visit the gym, going digital will soon become the most preferred way to keep fit.
            Along with fitness, music and Djing, Gagan also finds interest in singing and dancing. Making the session fun, energizing and interesting, Gagan is here to 'Fuel your muscles with Music.'
            If you are looking for a HIIT musical session with dance, Gagan will ensure that you are all sweaty and pumped up.</p>
</div>
         </div>
        </Modal>


        <Modal
          isOpen={modalIsOpen[6]}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Trainer Information"
        >
 
         <div className={styles.modalcontainer}>
            <div>
                <img src={dummy}/>
                <p className={styles.name}>Okie Dokey</p>
                <p className={styles.certi}>Certifications: ACE Certified, AFFA Certified, Milon certified,SPPINNG Certified</p>
                <p className={styles.exp}>Experience: 10 years</p>
            </div>
            <div>
             <p className={styles.bio}>Biography</p>
             <p className={styles.biodata}>Gagan is an artist! With a passion for fitness and music, he blends the two in a perfect way for an enchanted experience. An expert in Cycling, CrossFit and Strength & Conditioning training, Gagan is currently the best instructor in India who is also a DJ! With proficiency in conducting group classes, Gagan can provide the greatest cycling experience over music. Using his music skills, he ensures that the riders are constantly engaged and takes them on a journey.
            Gagan truly believes that online sessions are the 'Future of Fitness'. As people get busy with their schedules and are left with little or no time in hand to visit the gym, going digital will soon become the most preferred way to keep fit.
            Along with fitness, music and Djing, Gagan also finds interest in singing and dancing. Making the session fun, energizing and interesting, Gagan is here to 'Fuel your muscles with Music.'
            If you are looking for a HIIT musical session with dance, Gagan will ensure that you are all sweaty and pumped up.</p>
</div>
         </div>
        </Modal>


        <Modal
          isOpen={modalIsOpen[7]}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Trainer Information"
        >
 
         <div className={styles.modalcontainer}>
            <div>
                <img src={dummy}/>
                <p className={styles.name}>Dokie Dokie</p>
                <p className={styles.certi}>Certifications: ACE Certified, AFFA Certified, Milon certified,SPPINNG Certified</p>
                <p className={styles.exp}>Experience: 10 years</p>
            </div>
            <div>
             <p className={styles.bio}>Biography</p>
             <p className={styles.biodata}>Gagan is an artist! With a passion for fitness and music, he blends the two in a perfect way for an enchanted experience. An expert in Cycling, CrossFit and Strength & Conditioning training, Gagan is currently the best instructor in India who is also a DJ! With proficiency in conducting group classes, Gagan can provide the greatest cycling experience over music. Using his music skills, he ensures that the riders are constantly engaged and takes them on a journey.
            Gagan truly believes that online sessions are the 'Future of Fitness'. As people get busy with their schedules and are left with little or no time in hand to visit the gym, going digital will soon become the most preferred way to keep fit.
            Along with fitness, music and Djing, Gagan also finds interest in singing and dancing. Making the session fun, energizing and interesting, Gagan is here to 'Fuel your muscles with Music.'
            If you are looking for a HIIT musical session with dance, Gagan will ensure that you are all sweaty and pumped up.</p>
</div>
         </div>
        </Modal>


        <Modal
          isOpen={modalIsOpen[8]}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Trainer Information"
        >
 
         <div className={styles.modalcontainer}>
            <div>
                <img src={dummy}/>
                <p className={styles.name}>Bai Will SOon</p>
                <p className={styles.certi}>Certifications: ACE Certified, AFFA Certified, Milon certified,SPPINNG Certified</p>
                <p className={styles.exp}>Experience: 10 years</p>
            </div>
            <div>
             <p className={styles.bio}>Biography</p>
             <p className={styles.biodata}>Gagan is an artist! With a passion for fitness and music, he blends the two in a perfect way for an enchanted experience. An expert in Cycling, CrossFit and Strength & Conditioning training, Gagan is currently the best instructor in India who is also a DJ! With proficiency in conducting group classes, Gagan can provide the greatest cycling experience over music. Using his music skills, he ensures that the riders are constantly engaged and takes them on a journey.
            Gagan truly believes that online sessions are the 'Future of Fitness'. As people get busy with their schedules and are left with little or no time in hand to visit the gym, going digital will soon become the most preferred way to keep fit.
            Along with fitness, music and Djing, Gagan also finds interest in singing and dancing. Making the session fun, energizing and interesting, Gagan is here to 'Fuel your muscles with Music.'
            If you are looking for a HIIT musical session with dance, Gagan will ensure that you are all sweaty and pumped up.</p>
</div>
         </div>
        </Modal>


{/* modals */}

   <div id="test" className={styles.container}>

       <p>MEET THE TEAM</p>
       <div className={styles.flex_container}>
        <div>
        <img src={dummy} className={styles.head_img} alt="team"/>
            <div className={styles.text_container}>
            <p className={styles.head}>Some Text<span style={{cursor:'pointer'}}  onClick={()=>setIsOpen({...modalIsOpen,1:true})}><img src={Arrow}/></span></p> 
               <p className={styles.para}>Smaller Text</p> 

            </div>
        </div>
        
        <div>     
        <img src={dummy} className={styles.head_img} alt="team"/>
        <div className={styles.text_container}>
        <p className={styles.head}>Some Text<span style={{cursor:'pointer'}}  onClick={()=>setIsOpen({...modalIsOpen,2:true})}><img src={Arrow}/></span></p> 
               <p className={styles.para}>Smaller Text</p> 

            </div>

            </div>
      
        
        <div>
        <img src={dummy} className={styles.head_img} alt="team"/>
            <div className={styles.text_container}>
            <p className={styles.head}>Some Text<span style={{cursor:'pointer'}}  onClick={()=>setIsOpen({...modalIsOpen,3:true})}><img src={Arrow}/></span></p> 
               <p className={styles.para}>Smaller Text</p> 

            </div>
        </div>

        <div>
        <img src={dummy} className={styles.head_img} alt="team"/>
            <div className={styles.text_container}>
            <p className={styles.head}>Some Text<span style={{cursor:'pointer'}}  onClick={()=>setIsOpen({...modalIsOpen,4:true})}><img src={Arrow}/></span></p> 
               <p className={styles.para}>Smaller Text</p> 

            </div>
        </div>    

        <div>
        <img src={dummy} className={styles.head_img} alt="team"/>
            <div className={styles.text_container}>
            <p className={styles.head}>Some Text<span style={{cursor:'pointer'}}  onClick={()=>setIsOpen({...modalIsOpen,5:true})}><img src={Arrow}/></span></p> 
               <p className={styles.para}>Smaller Text</p> 

            </div>
        </div>    

        <div>
        <img src={dummy} className={styles.head_img} alt="team"/>
            <div className={styles.text_container}>
            <p className={styles.head}>Some Text<span style={{cursor:'pointer'}}  onClick={()=>setIsOpen({...modalIsOpen,6:true})}><img src={Arrow}/></span></p> 
               <p className={styles.para}>Smaller Text</p> 

            </div>
        </div>    
        <div>
        <img src={dummy} className={styles.head_img} alt="team"/>
            <div className={styles.text_container}>
            <p className={styles.head}>Some Text<span style={{cursor:'pointer'}}  onClick={()=>setIsOpen({...modalIsOpen,7:true})}><img src={Arrow}/></span></p> 
               <p className={styles.para}>Smaller Text</p> 

            </div>
        </div>    
        <div>
        <img src={dummy} className={styles.head_img} alt="team"/>
            <div className={styles.text_container}>
            <p className={styles.head}>Some Text<span style={{cursor:'pointer'}}  onClick={()=>setIsOpen({...modalIsOpen,8:true})}><img src={Arrow}/></span></p> 
               <p className={styles.para}>Smaller Text</p> 

            </div>
        </div>    
       </div>
   </div>

    </Layout>
 



  // </Layout>
)
    }
export default Team

