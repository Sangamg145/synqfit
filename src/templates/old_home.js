// import React from 'react'
// import { graphql } from 'gatsby'

// import PageHeader from '../components/PageHeader'
// import Content from '../components/Content'
// import Layout from '../components/Layout'
// import "./HomePage.css"

// import logo from "../assets/images/logo.png"

// import burg from "../assets/images/burg.png"
// import insta from "../assets/images/instagram.png"
// import fb from "../assets/images/facebook.png"
// import tweet from "../assets/images/twitter.png"
// import apple from "../assets/images/apple_store.png"
// import android from "../assets/images/android_store.png"

// import ic_calender from "../assets/images/ic-calendar.png"
// import ic_cycle from "../assets/images/ic-cycle.png"
// import ic_diverse from "../assets/images/ic-diverse.png"
// import ic_home from "../assets/images/ic-home.png"
// import ic_video from "../assets/images/ic-video-library.png"
// import ic_trainers from "../assets/images/ic-trainers.png"

// import one from "../assets/images/01.png"
// import two from "../assets/images/02.png"
// import three from "../assets/images/03.png"
// import four from "../assets/images/04.png"
// import five from "../assets/images/05.png"
// import six from "../assets/images/06.png"

// import card_one from "../assets/images/card_1.png"
// import card_two from "../assets/images/img-2.png"
// import card_three from "../assets/images/img-3.png"

// // mobile Content
// import mob_logo from "../assets/mob_images/logo-compact.png"
// import mob_burg from "../assets/mob_images/mobile_burg.png"
// import cycle_lady from "../assets/mob_images/bike-screen-mobile.png"


// import mob_ic_calender from "../assets/mob_images/ic-calendar-mobile.png"
// import mob_ic_cycle from "../assets/mob_images/ic-cycle-mobile.png"
// import mob_ic_diverse from "../assets/mob_images/ic-diverse-mobile.png"
// import mob_ic_home from "../assets/mob_images/ic-home-mobile.png"
// import mob_ic_video from "../assets/mob_images/ic-video-library-mobile.png"
// import mob_ic_trainers from "../assets/mob_images/ic-trainers-mobile.png"



// import mob_one from "../assets/mob_images/01_mobile.jpg"
// import mob_two from "../assets/mob_images/02_mobile.jpg"
// import mob_three from "../assets/mob_images/03_mobile.jpg"
// import mob_four from "../assets/mob_images/04_mobile.jpg"
// import mob_five from "../assets/mob_images/05_mobile.jpg"
// import mob_six from "../assets/mob_images/06_mobile.jpg"


// //here over
// import { Link } from 'gatsby'

// // Export Template for use in CMS preview
// export const HomePageTemplate = ({ title, subtitle, featuredImage, body }) => (
//   <main className="Home">
//     <PageHeader
//       large
//       title={title}
//       subtitle={subtitle}
//       backgroundImage={featuredImage}
//     />

//     <section className="section">
//       <div className="container">
//         <Content source={body} />
//       </div>
//     </section>
//   </main>
// )

// // Export Default HomePage for front-end
// const HomePage = ({ data: { page } }) => (
//   // <Layout meta={page.frontmatter.meta || false}>
   
//    <>
   
//     <div class="browse_plan">
//     <div className="header">
//       <Link to="/"><div className='logo'>
//       <img src={logo} alt="logo" />
//       </div></Link>
//       <div className="navheader">
//        <ul>
//        <li><Link to="/#bike">BIKE</Link></li>
//        <li><Link to="/#apps">APP</Link></li>
//        <li><Link to="/#community">COMMUNITY</Link></li>

//        </ul>
//       </div>
      
//       <div className="hamburg">
//       <img src={burg} alt="burger" />

//       </div>
      
   
//     </div>

//     {/* mobile menu */}
//     <div className="header_mobile">
//       <div className='mobile_logo'>
//       <img src={mob_logo} alt="logo"/>
//       </div>
      
      
//       <div className="mobile_hamburg">
//       <img src={mob_burg} alt="burger" />

//       </div>
      
   
//     </div>
//     {/* mobile menu over */}
// <div className="browse_plan_container">
//   <div></div>
//   <div>
//       <p className="plan_texts">
//       Immerse yourself in a wide range of <br/>interactive, trainer led workouts <br/> streamed live to your home on your <br/> Synq.Fit bike. 
//       </p>
//       <button className="buy_bikes"><Link  style={{textDecoration:'none',color:'#dbdfeb'}}to="/product/">Pre-Book Now</Link></button>
//       </div>
//       </div>
//     </div>


// {/* cycle lady */}

//   <div className="cycle_lady">
//   <div>
//   <p className="head">
//   Connected,<br/>
//   Interactive,<br/>
//   Feature Packed.<br/>
//   </p>
//   <img src={cycle_lady} className="mob_cycle_lady" alt="lady"/>

//   {/* <p className='para'>
//   Immerse yourself in a wide range of interactive, <br/>trainer-led workouts streamed live to the comfort<br/> and safety of your home on your very own<br/>
// Synq.Fit bike.
//   </p> */}
//   <p className='para'>
//   Synq.fit goes much beyond being workout equipment. It is a whole new way of experiencing connected fitness in this digital world.
//   </p>

// {/* <p className='para'>
// Immerse yourself in a wide range of interactive, trainer-led workouts streamed live to the comfort and safety of your home on your very own
// Synq.Fit bike.
// </p> */}

//   <button class="contact_us"><Link style={{textDecoration:'none',color:'white'}}to="/#contact">Contact Us</Link></button>
//   </div>
  
//   <div className="extra_div">
    
//   </div>

// </div>
// {/* cycle lady over*/}



// {/*cycle types*/}
// <div id="bike" className="cycles_types">

//   <div className="cycle_1 cycles_list">
//     <img className="pc_cycles" src={one} alt=""/>
//     <img className="mob_cycles" src={mob_one} alt=""/>


//     <p className="image_para">15.6" Interactive<br/> Touchscreen Display</p>
//     <p className="hover_data">Immersive, 1080p resolution screen brings the studio to you, 15.6in display draws you in like a one-on-one session with the instructors. Feel the high energy of group workouts, real-time interaction with other riders and get instant feedback on your performance. </p>

//   </div>
//   <div className="cycle_2 cycles_list">
//     <img className="pc_cycles" src={two} alt=""/>
//     <img className="mob_cycles" src={mob_two} alt=""/>

//     <p className="image_para">Compact yet<br/> heavy duty</p>
//     <p className="hover_data">With dimensions of LxB, the equipment will easily fit even in small places and convert them into your personal fitness studio. The design makes it quite easy to move the cycle around the house. Sturdy high-quality build quality ensures years of hassle-free operation. </p>


//   </div>
//   <div className="cycle_3 cycles_list">
//     <img className="pc_cycles" src={three} alt=""/>
//     <img className="mob_cycles" src={mob_three} alt=""/>

//     <p className="image_para">Connect to all<br/>your fitness devices</p>
//     <p className="hover_data">Synq.fit connects to WiFi and Bluetooth, bringing all your devices together. Connect Synq.fit to your fitness wearable’s or even play your favorite music. Let all your connected devices push you to achieve your fitness goals.  </p>
//   </div>
//   <div className="cycle_4 cycles_list">
//     <img className="pc_cycles"  src={four} alt=""/>
//     <img className="mob_cycles" src={mob_four} alt=""/>

//     <p className="image_para">Belt Drive</p>
//     <p className="hover_data">Bringing you the latest technology is exercise bikes - the belt drive. This makes the cycle extremely silent allowing you to have workout any time. With no noise from the cycle, immerse yourself in peaceful music and go on a long ride any time.</p>


//   </div>
//   <div className="cycle_5 cycles_list">
//     <img className="pc_cycles" src={five} alt=""/>
//     <img className="mob_cycles" src={mob_five} alt=""/>

//     <p className="image_para">Multi-Level <br/>Magnetic Resistance</p>
//     <p className="hover_data">The Multi-Level Magnetic Resistance provides consistent resistance and enables the user to make accurate adjustments to the resistance levels. Zero friction point is the best part - it requires minimal maintenance.</p>


//   </div>
//   <div className="cycle_6 cycles_list">
//     <img  className="pc_cycles" src={six} alt=""/>
//     <img className="mob_cycles" src={mob_six} alt=""/>

//     <p className="image_para">Highly<br/>Adjustable</p>
//     <p className="hover_data">The multipoint adjustment system allows you to change the position of the handlebar, the seat and even the screen. This makes the product useful for your entire family; motivating each one to take up exercise and achieve their unique fitness goals</p>


//   </div>

// </div>

// {/*cycle types over*/}

// {/*ride everyday*/}

// <div id="apps" className="ride_everyday">
//   <div>
//   <p className="head">
//   Ride Every Day.<br/>
//   Better Every Day.<br/>
//   </p>

//   {/* <p className='para'>
//   Synq.Fit studio classes are just like going to the<br/>gym, except you get to stay at home. These<br/>classes with our trainers are meant to pump you <br/>up and push your own limits.
//   </p> */}
//   <p className='para'>
//   Synq.Fit studio classes are just like going to the gym, except you get to stay at home. These classes with our trainers are meant to pump you up and push your own limits.
//   </p>

//   <button class="contact_us"><Link  style={{textDecoration:'none',color:'#dbdfeb'}}to="/product/">Pre-Book Now</Link></button>
//   </div>
  
//   <div>
    
//   </div>

// </div>
// {/* ride everyday over */}


// {/* features */}
// <div className="features">
// <h1>
//   Features
// </h1>
// <div className="feature_list">
//   <div className="item_1">
//     <div className="feature_list_img">
//   <img className="pc_feature" src={ic_calender}/>
//   <img className="mob_feature" src={mob_ic_calender}/>
//   </div>
//  <p className="head">Live Sessions Everyday</p>
//  <p className="para">Feel the energy of a live workouts, interact real-time with the trainers and get instant feedback.</p>
//   </div>
//   <div className="item_2">
//     <div className="feature_list_img">
//       <img className="pc_feature" src={ic_home}/>
//       <img className="mob_feature" src={mob_ic_home}/>

//     </div>

//     <p className="head">Gym for the Entire Family</p>
//     <p className="para">Be it your mother, wife, brother, son  or father - create and track individual profiles and data with Synq.Fit bike.</p>
//   </div>

//   <div className="item_3">
//     <div className="feature_list_img">
//       <img className="pc_feature" src={ic_video}/>
//       <img className="mob_feature" src={mob_ic_video}/>

//     </div>

//     <p className="head">On-demand Video Library</p>
//     <p className="para">In the mood for a quick 5-mins session with an instructor or a 90-mins session on a scenic mountain? Our library has it all.</p>
  
//   </div>
//   <div className="item_4">
//     <div className="feature_list_img">
//       <img className="pc_feature" src={ic_cycle}/>
//       <img className="mob_feature" src={mob_ic_cycle}/>

//     </div>
//     <p className="head">Join the Movement</p>
//     <p className="para">Connect with friends and riders from across the world, go at your own pace, compete at different levels.</p>
  
//   </div>
//   <div className="item_5">
//     <div className="feature_list_img">
//       <img className="pc_feature" src={ic_trainers}/>
//       <img className="mob_feature" src={mob_ic_trainers}/>

//     </div>
    
//     <p className="head">Best-in-class Instructors</p>
//     <p className="para">Our motivating instructors ensure that you keep coming back for more of what you like and experience new ones.</p>
  
//   </div>
//   <div className="item_6">
//     <div className="feature_list_img">
//       <img className="pc_feature"  src={ic_diverse}/>
//       <img className="mob_feature" src={mob_ic_diverse}/>

//     </div>
//     <p className="head">Diverse Class Types</p>
//     <p className="para">Classes ranging from energizing HIIT training sessions to stress-busting easy rides with hand-picked music to boast.</p>
  
//   </div>

// </div>
// </div>
// {/* features over*/}




// {/*trainers*/}

// <div  className="ride_everyday">
//   <div>
//   <p className="head">
//   Meet the trainers
//   </p>

//   {/* <p className='para'>
//   Synq.Fit studio classes are just like going to the<br/>gym, except you get to stay at home. These<br/>classes with our trainers are meant to pump you <br/>up and push your own limits.
//   </p> */}
//   <p className='para'>
//   Synq.Fit studio classes are just like going to the gym, except you get to stay at home. These classes with our trainers are meant to pump you up and push your own limits.
//   </p>

//   <button class="contact_us"><Link  style={{textDecoration:'none',color:'#dbdfeb'}}to="/trainers/">Trainers Profile</Link></button>
//   </div>
  
//   <div>
    
//   </div>

// </div>
// {/* trainers over */}





// {/* testimonial */}
// <div className="testimonial">
//   <h1 className="head">TESTIMONIALS</h1>
//   {/* <p className="para">
//   "It's just amazing. SynqFit is both attractive and<br/>highly adaptable.I have gotten at least 50 times<br/>the value from SynqFit. If you want real<br/>marketing that works and effective<br/>implementation - SynqFit's got you covered."
//   </p> */}
//     <p className="para">
//   "It's just amazing. SynqFit is both attractive and highly adaptable.I have gotten at least 50 times the value from SynqFit. If you want real marketing that works and effective implementation - SynqFit's got you covered."
//   </p>

//   <p className="para_name">- Garcia X</p>

// </div>
// {/* testimonial over*/}


// {/* community feature */}

// <div className="comm_feature" id="community">
//   <h1 className="heading">Community Features</h1>
//   <div className="card_features">
// <div className="card">
//   <div className="card_img">
//   <img src={card_one} alt="" className="feature_img"/>
//   </div>
//   <p className="card_head">Ride with Friends</p>
//   <p className="card_para">Connect with your friends on Synq.Fit and keep the spirits high.</p>
// </div>

// <div className="card">
// <div className="card_img">
//   <img src={card_two} alt="" className="feature_img"/>
// </div>
//   <p className="card_head">Compete with Strangers</p>
//   <p className="card_para">Select a partner who has a profile <br/>similar to yours in terms of age,<br/> weight, gender and difficulty level.<br/> When it comes to testing your strength and stamina,
// <br/>it’s GAME ON.</p>
// </div>

// <div className="card">
// <div className="card_img">
//   <img src={card_three} alt="" className="feature_img"/>
// </div>
//   <p className="card_head">Get mesmerized</p>
//   <p className="card_para">Go on long rides with scenic views<br/> and pleasant music that will help<br/> you unwind.</p>
// </div>

// </div>
// </div>

// {/* community feature over*/}

// {/*fit everyday */}
// <div className="fit_everyday">
// <h1 className="fit_heading">Better Everyday</h1>
// </div>
// {/*fit everyday over*/}


// {/*Contact Us*/}
// <div className="contact_us_form" id="contact">
//   <div className="contact_text">
//     <p className="form_heading">Contact Us</p>
//     <p className="form_paragraph">Feel free to reach out to us and get to<br/> know more about Synq.Fit.
//       <br/><br/>Join the synq community now!</p>
//       <p className="form_label">EMAIL</p>
//       <p className="form_value">help@synq.fit</p>
//       <p className="form_label">PHONE NUMBER</p>
//       <p className="form_value">+91-8700128721</p>
//       <span className="social_icons">
//         <img src={tweet}/>
//         <img src={insta}/>
//         <img src={fb}/>
//       </span>
//       <p className="form_copyright">© Synq.Fit 2020</p>
//       <p className="mobile_footer_content">Or leave your contact details and we’ll get back to you.</p>
      
//   </div>

//   <div className="form_contact_us">
//     <input className="form_input" placeholder="Name"/>
//     <input className="form_input" placeholder="Email"/>
//     <input className="form_input" placeholder="Mobile number"/>
//     <input className="form_input" placeholder="City"/>
//     <textarea className="form_text_area" placeholder="Message" rows="10" cols="10"/>
//     <button className="form_button">Submit</button>

//   </div>


// </div>
// {/*Contact Us over*/}


// <div className="footer">
// <div className="footer_menu">
//   <div className="footer_menu_1">
    
//     <div>
//       <p className="footer_heading">Synq.fit bike</p>
//       <ul>
//         <li>Buy the Bike</li>
//         <li>Technical Details</li>
//         <li>Product Review & Testimonial</li>
//         <li>Synq.fit Plans</li>

//       </ul>
//     </div>
    
//     <div>      
//       <p className="footer_heading">About</p>
//       <ul>
//         <li>About Us</li>
//         <li>Trainers</li>
//         <li>Team</li>
//         <li><Link to="/blog/">Blog</Link></li>



//       </ul>
//     </div>
    
//     <div>      
//       <p className="footer_heading">Support</p>
//       <ul>
//       <li><Link to="/contact/">Contact Us</Link></li>
//         <li>FAQs</li>
//         <li>Bike Warranty</li>
//         <li>Privacy Policy</li>
//         <li>Terms and Conditions</li>

        
//       </ul>
//     </div>

    
//   </div>

//   <div>
//   <p className="footer_heading right_footer">Sign up to receive product and services notifications</p>
//   <input className="footer_input" type="email"/>
//   <button className="footer_form_button">Submit</button>

// <p className="footer_heading right_footer">
// Download our App
// </p>
// <img className="mobile_img" src={apple} alt="apple"/>
// <img className="mobile_img" src={android} alt="android" />

//   </div>

// </div>

// <div className="footer_text">
//   <h2>Terms And Conditions</h2>
//   <span >
//   One Stop Solution for your workout from home<br/>
// It is a digital world and fitness is not behind. In this connected era, it has become essential to combine physical and digital worlds, and Synq.Fit merges these seamlessly for you. We are a new age connected fitness company that keeps you engaged in instructor-led fitness activities. Keep laziness and stress at a bay and leave it to Synq.Fit to solve all your fitness needs with our unique curated program.<br/>
// Synq.Fit helps you to maintain these fitness levels by bringing in best in class workout programs at your home. With live sessions every day, Synq.Fit coverts your living room into an energetic fitness studio motivating you to sweat it out. It is essential to maintain a basic fitness level to improve work efficiency and productivity, and Synq.Fit is an ultimate solution to all your cardio issues. <br/>
// Bringing in world class technology, Synq.Fit helps you achieve your fitness goals from the comfort of your living room. The membership plans are especially curated to serve all your needs. <br/>
// In the last few years, work from home has gained momentum in India. The purpose of this shift was to keep individuals motivated and improve their work life balance, but this nature of work also blurred the lines between personal and professional lives. While work from home is considered a good option, it can lead to many fitness issues in individuals. Lack of physical activity can lead to obesity, type 2 diabetes, heart disease and even cancer.<br/>
// Synq.Fit also improves your work life balance by managing your social as well as physical fitness appetite. The program not only adds fun into fitness, it also helps you maintain a good social circle by interacting with friends and family.<br/>
// Synq.fit is not just home cardio solution, but a fitness movement. Our users not just stay healthy and fit, but also become a part of the bigger riding movement that aims to make India healthier. <br/>
// </span>
// </div>
// </div>
//     </>
 



//   // </Layout>
// )

// export default HomePage

// export const pageQuery = graphql`
//   ## Query for HomePage data
//   ## Use GraphiQL interface (http://localhost:8000/___graphql)
//   ## $id is processed via gatsby-node.js
//   ## query name must be unique to this file
//   query HomePage($id: String!) {
//     page: markdownRemark(id: { eq: $id }) {
//       ...Meta
//       html
//       frontmatter {
//         title
//         subtitle
//         featuredImage
//       }
//     }
//   }
// `
