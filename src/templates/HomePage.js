import React,{useState} from 'react'
import { graphql } from 'gatsby'

import PageHeader from '../components/PageHeader'
import Content from '../components/Content'
import Layout from '../components/Layout'
import "./HomePage.css"

import logo from "../assets/images/logo.png"

import burg from "../assets/images/burg.png"
import insta from "../assets/images/instagram.png"
import fb from "../assets/images/facebook.png"
import linkedin from "../assets/images/linkedin_icon.png"
import apple from "../assets/images/apple_store.png"
import android from "../assets/images/android_store.png"

import ic_calender from "../assets/images/ic-calendar.png"
import ic_cycle from "../assets/images/ic-cycle.png"
import ic_diverse from "../assets/images/ic-diverse.png"
import ic_home from "../assets/images/ic-home.png"
import ic_video from "../assets/images/ic-video-library.png"
import ic_trainers from "../assets/images/ic-trainers.png"

import one from "../assets/images/01.png"
import two from "../assets/images/02.jpg"
import three from "../assets/images/03.png"
import four from "../assets/images/04.png"
import five from "../assets/images/05.png"
import six from "../assets/images/06.png"

import card_one from "../assets/images/card_1.png"
import card_two from "../assets/images/img-2.png"
import card_three from "../assets/images/img-3.png"
 
// mobile Content
import mob_logo from "../assets/mob_images/logo-compact.png"
import mob_burg from "../assets/mob_images/mobile_burg.png"
// import cycle_lady from "../assets/mob_images/bike-screen-mobile.png"
import cycle_lady from "../assets/mob_images/bike-screen-mobile.svg"

 
import mob_ic_calender from "../assets/mob_images/ic-calendar-mobile.png"
import mob_ic_cycle from "../assets/mob_images/ic-cycle-mobile.png"
import mob_ic_diverse from "../assets/mob_images/ic-diverse-mobile.png"
import mob_ic_home from "../assets/mob_images/ic-home-mobile.png"
import mob_ic_video from "../assets/mob_images/ic-video-library-mobile.png"
import mob_ic_trainers from "../assets/mob_images/ic-trainers-mobile.png"

 

import mob_one from "../assets/mob_images/01_mobile.jpg"
import mob_two from "../assets/mob_images/02_mobile.jpg"
import mob_three from "../assets/mob_images/03_mobile.jpg"
import mob_four from "../assets/mob_images/04_mobile.jpg"
import mob_five from "../assets/mob_images/05_mobile.jpg"
import mob_six from "../assets/mob_images/06_mobile.jpg"
import Trainers from "../assets/images/trainer.png"



import BackgroundImage from 'gatsby-background-image'

import PhoneCode from 'react-phone-code';

import { RiSendPlaneFill } from "react-icons/ri";
import Zoom from '@material-ui/core/Zoom';
import Grow from '@material-ui/core/Grow';
import Fade from 'react-reveal/Fade';


 
//here over
import { Link } from 'gatsby'
import axios from "axios"
import Spin from "../components/Spinner"
import PhoneInput, {isValidPhoneNumber} from 'react-phone-number-input'
import 'react-phone-number-input/style.css'
import  {useForm}  from 'react-hook-form'
import { ErrorMessage } from "@hookform/error-message";
import { navigate } from "gatsby"

// On Load Popup

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Alert from '@material-ui/lab/Alert';

// Card
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid'

import video_1 from '../assets/images/product/video1.jpg'
import video_2 from '../assets/images/product/video2.jpg'
import video_3 from '../assets/images/product/video3.jpg'
import video_4 from '../assets/images/product/video4.jpg'
import join_btn from '../assets/images/product/btn.png'

import { Player } from "video-react";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

import "../../node_modules/video-react/dist/video-react.css";
 
import ride_img from '../assets/mob_images/ride.png'
import {Box} from '@material-ui/core'


import news1 from '../assets/images/news1.png'
import news2 from '../assets/images/news2.png'
import news3 from '../assets/images/news3.png'
import news4 from '../assets/images/news4.png'
import news5 from '../assets/images/news5.png'

import {Carousel} from 'react-bootstrap'

import WhatsAppWidget from 'react-whatsapp-widget'
import '../../node_modules/react-whatsapp-widget/dist/index.css'

import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
// Export Template for use in CMS preview
export const HomePageTemplate = ({ title, subtitle, featuredImage, body }) => (
  <main className="Home">
    <PageHeader
      large
      title={title}
      subtitle={subtitle}
      backgroundImage={featuredImage}
    />

    <section className="section">
      <div className="container">
        <Content source={body} />
      </div>
    </section>
  </main>
)



// Export Default HomePage for front-end
const HomePage = ({ data}) =>
{

  const [ invite, setInvite] = useState('')
  const [success, setSuccess] = useState(null)
  const [error, setError] = useState(null)
  const [value, setValue] = useState()
  const { register, handleSubmit, errors } = useForm({criteriaMode: "all"})
const [formdata,setData]=useState({
  username:'',
  phone:'',
  city:'',
  comment:'',
  email:'',
});
// console.log(formdata)
const [loaded,setLoaded]=useState(true);


const formsubmit = formdata =>{
  //  event.preventDefault();
   setLoaded(false);
//   const name=event.target.elements.username.value;
//   const mobile=event.target.elements.phone.value;
//   const city=event.target.elements.city.value;
//   const message=event.target.elements.comment.value;
//   const email=event.target.elements.email.value;

const data = {
  name:formdata.username,
  mobile: value,
  city:formdata.city,
  message:formdata.comment,
  email:formdata.email,
};
axios.post('https://admin.synq.fit/core/api/contacts/',data).then(response=>{
  console.log(response);
  setLoaded(true);
  // setSuccess('Thank you for the information , We will get back to you soon.')
  navigate('/thankyou')
  setData({username:'',
  phone:'',
  city:'',
  comment:'', 
  email:'',
})
setValue('')
}).catch(err=>{
  setLoaded(true);
  setError('Something went wrong.')
  setData({username:'',
  phone:'',
  city:'',
  comment:'',
  email:'',
})
})
 }

 const formData = (e)=>{
  setData({...formdata,[e.target.name]:e.target.value})
  // console.log(e.target.name,e.target.value)
 }

  // const sources = [
  //   data.better_everyday.childImageSharp.fluid,
  //   data.image_landing_mini.childImageSharp.fluid
  // ]
//resolutions
  const first_sources = [
    data.image_landing.childImageSharp.fluid,
    {
      ...data.image_landing.childImageSharp.fluid,
      media: `(min-width: 600px)`,
    },
    data.image_landing_mini.childImageSharp.fluid,
    {
      ...data.image_landing_mini.childImageSharp.fluid,
      media: `(max-width: 480px)`,
    },
  ]


  // const second_source = [
  //   data.bike_hero.childImageSharp.fluid,
  //   {
  //     ...data.bike_hero.childImageSharp.fluid,
  //     media: `(min-width: 600px)`,
  //   },
  //   data.image_landing_mini.childImageSharp.fluid,
  //   {
  //     ...data.image_landing_mini.childImageSharp.fluid,
  //     media: `(max-width: 480px)`,
  //   },
  // ]


  const third_source = [
    data.image_app.childImageSharp.fluid,
    {
      ...data.image_app.childImageSharp.fluid,
      media: `(min-width: 600px)`,
    },
    data.image_app_mini.childImageSharp.fluid,
    {
      ...data.image_app_mini.childImageSharp.fluid,
      media: `(max-width: 480px)`,
    },
  ]


  const fourth_source = [
    data.better_everyday.childImageSharp.fluid,
    {
      ...data.better_everyday.childImageSharp.fluid,
      media: `(min-width: 600px)`,
    },
    data.better_everyday_mini.childImageSharp.fluid,
    {
      ...data.better_everyday_mini.childImageSharp.fluid,
      media: `(max-width: 480px)`,
    },
  ]
  //over

  const onInviteSubmit =(event) => {
    event.preventDefault();
    setLoaded(false);
    const data = {
      "email": invite,
      "leads_type": 1
    }
    console.log(data)

    axios.post(
      'https://admin.synq.fit/core/api/leads/',
      data,
    )
    .then( res => {
      console.log('response', res)
      setLoaded(true);
      setInvite('')
      setSuccess('Thank you for choosing us, Our team will get back to you soon')
     

    })
    .catch(err => {
      console.log(err)
      setLoaded(true);
      setInvite('')
      setError('something went wrong!')
    })
  }

  // Handling Popup

  const [open, setOpen] = React.useState(false);
  const [email, setEmail]=React.useState(null);
  const [emailError, setEmailError] = React.useState(null)
  const [emailSuccess, setEmailSuccess] = React.useState(null)


  const handlePopupClose = () => {
    setOpen(false);
  };


  React.useEffect(() => {
    const timeLimit = 5 //seconds;
    let s = 0;
    const interval = setInterval(() => {
      s++;
      if(s === timeLimit){
        clearInterval(interval); 
        console.log("pop will compiong")
        setOpen(true)
      }
    },1000)

    // return () => clearInterval()
  },[])

  const validation = ()=> {

    let emailError = ""
    let emailpattern = new RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
    if(!email){
        emailError = "Please enter a email!"
     } else if(!emailpattern.test(email)){
        emailError = "Please enter a valid email!"   
     }

     if(emailError){
       setEmailError(emailError)
       return false
     }

     return true
  }

  const handleEmailSubmit = () => {
    console.log("I Clicked", email)
    const isValid = validation()
    if(isValid){
      console.log("form submit", email)
      const timeLimit = 2 //seconds;
      let s = 0;
      setEmailSuccess("Thank you for subscribing us !!")
      const interval = setInterval(() => {
        s++;
        if(s === timeLimit){
          clearInterval(interval); 
          console.log("pop will compiong")
          setOpen(false)
        }
      },1000)
      
    }
    
  }

  //Handling video model

  const [isOpen, setIsOpen] = useState(false)
  const [isOpen2, setIsOpen2] = useState(false)
  const [isOpen3, setIsOpen3] = useState(false)
  const [isOpen4, setIsOpen4] = useState(false)

  const handleVideoOpen = () => {
    setIsOpen(true);
  };
  const handleVideoClose = () => {
    setIsOpen(false);
  };

  // video2

  const handleVideoOpen2 = () => {
    setIsOpen2(true);
  };
  const handleVideoClose2 = () => {
    setIsOpen2(false);
  };

  // video3

  const handleVideoOpen3 = () => {
    setIsOpen3(true);
  };
  const handleVideoClose3 = () => {
    setIsOpen3(false);
  };

  //video4

  const handleVideoOpen4 = () => {
    setIsOpen4(true);
  };
  const handleVideoClose4 = () => {
    setIsOpen4(false);
  };

return(
  // <Layout meta={page.frontmatter.meta || false}>
   
   <Layout>
    {/* <div class="browse_plan">
    <div className="browse_plan_container">
  <div></div>
  <div>
      <p className="plan_texts">
      Immerse yourself in a wide range of <br/>interactive, trainer led workouts <br/> streamed live to your home on your <br/> Synq.Fit bike. 
      </p>
      <button className="buy_bikes"><Link  style={{textDecoration:'none',color:'#dbdfeb'}}to="/product/">Pre-Book Now</Link></button>
      </div>
      </div>
    </div> 
 */}
{!loaded&&<Spin/>}

{/* 
<BackgroundImage
          Tag="div"
          className="browse_plan"
          // fluid={data.image_landing.childImageSharp.fluid}
          //fluid={first_sources}
          >
<div className="browse_plan_container">
  <div></div>
  <div>
      <p className="plan_texts">
      Immerse yourself in a wide range of <br/>interactive, gautam trainer led workouts <br/> streamed live to your home on your <br/> Synq.Fit bike. 
      </p>
      
     
      

      <button className="buy_bikes"><Link  style={{textDecoration:'none',color:'#dbdfeb'}}to="/product/">Pre-Book Now</Link></button>
      </div>
      </div> 
  </BackgroundImage> */}

  {/* Dialog Handling  */}

      {/* <Dialog open={open} onClose={handlePopupClose} aria-labelledby="form-dialog-title">
        {
          emailSuccess
          ?
          <>
            <DialogContent>
              <Alert severity="success"> {emailSuccess} </Alert>
            </DialogContent>
            <DialogActions>
            <Button className="popup_btn" onClick={handlePopupClose} color="primary">
              Close
            </Button>
          </DialogActions>
          </>
          :
          <>
          <DialogTitle id="form-dialog-title">
            <span  className="popup_title">
              Subscribe
            </span>
          </DialogTitle>
          <DialogContent>
            <DialogContentText >
            <span className="popup_desc">
                To subscribe to this website, please enter your email address here. We will send updates
                  occasionally.
            </span>
            </DialogContentText>
            {emailError ? <Alert severity="error"> {emailError} </Alert> : ''}
            <TextField
              autoFocus
              required
              margin="dense"
              id="name"
              label="Email Address"
              type="email"
              fullWidth
              onChange = {e => setEmail(e.target.value)}
              error={emailError}
            /> 
          </DialogContent>
          <DialogActions>
            <Button className="popup_btn" onClick={handlePopupClose} color="primary">
              Cancel
            </Button>
            <Button className="popup_btn" onClick={handleEmailSubmit} color="primary">
              Subscribe
            </Button>
          </DialogActions>
          </>
        }
      </Dialog> */}

  <WhatsAppWidget 
    phoneNumber='918700128721'
    message	='Hey There Fitness enthusiast. We are here to help you.'
  />
  <div className="browse_plan_container"> 
  <div></div>
      <div>
       
        <Grow in style={{ transitionDelay: '500ms' }}>
        <p className="plan_texts">
        Immerse yourself in a wide range of <br/>interactive, trainer led workouts <br/> streamed live to your home on your <br/> Synq.Fit bike. 
        </p>
        </Grow>
         
        {/* {success ? <p className="plan_texts" style={{fontSize: '16px', paddingTop: '200px', padding: '20px 0'}}>{success}</p>: ''}
        {error ? <p className="plan_texts" style={{fontSize: '16px', paddingTop: '200px', color: 'red', padding: '20px 0'}}>{error}</p>: ''} */}
        <Fade bottom style={{ transitionDelay: '1000ms' }} >
        <form className="form" onSubmit={onInviteSubmit}>
          {/* <input className="browse_form" type="text" placeholder="Get an invite"  onChange={e => {setInvite(e.target.value)}} name="invite_email"/> */}
          <button className="buy_bikes" type="submit"><Link  to="/product/" style={{color: '#fff', textDecoration: 'none'}}>Get the Bike</Link></button> 
        </form>
        </Fade>
        
        {/* <button className="buy_bikes"><Link  to="#contact">Pre-Book Now</Link></button> */}
        </div>
      </div> 
      



{/* cycle lady */}
  
    <div className="cycle_lady">
      <div>
        <Fade bottom>
          <p className="head">
            Connected,<br/>
            Interactive,<br/>
            Feature Packed.<br/>
          </p>
        </Fade> 
    <img src={cycle_lady} className="mob_cycle_lady" alt="lady"/>

    {/* <p className='para'>
    Immerse yourself in a wide range of interactive, <br/>trainer-led workouts streamed live to the comfort<br/> and safety of your home on your very own<br/>
  Synq.Fit bike.
    </p> */}
    <Fade bottom>
      <p className='para'>
      Synq.fit goes much beyond being workout equipment. It is a whole new way of experiencing connected fitness in this digital world.
      </p>

    </Fade>
  {/* <p className='para'>
  Immerse yourself in a wide range of interactive, trainer-led workouts streamed live to the comfort and safety of your home on your very own
  Synq.Fit bike.
  </p> */}
        {/* {success ? <p className="para" style={{fontSize: '16px', padding: '20px 0', }}>{success}</p>: ''}
        {error ? <p className="para" style={{fontSize: '16px', color: 'red', padding: '20px 0'}}>{error}</p>: ''} */}
      <Fade bottom>
      <form className="form" style={{marginRight: '0', textAlign: 'center'}}>
          {/* <input className="browse_form" style={{backgroundColor: 'transparent !important'}}   onChange={e => {setInvite(e.target.value)}} name="invite_email" type="text"  placeholder="Get an invite"/> */}
          <button className="buy_bikes" style={{marginRight: 'auto'}} type="submit"><Link  to="#contact" style={{color: '#fff', textDecoration: 'none'}}>Get Invite</Link></button>
      </form>
      </Fade>
    <Fade bottom>
      <button class="contact_us" style={{display: 'none'}}><Link style={{textDecoration:'none',color:'white'}}to="/#contact">Contact Us</Link></button>
    </Fade>
    </div>
    
    <Fade right>
      <div className="extra_div">
      </div>
    </Fade>
      
   

  </div>
  
{/* cycle lady over*/}



{/*cycle types*/}
<div id="bike" className="cycles_types">

  <Fade bottom>
    <div className="cycle_1 cycles_list">
      <img className="pc_cycles" src={one} alt=""/>
      <img className="mob_cycles" src={mob_one} alt=""/>


      <p className="image_para">HD Android<br/>Based Tablet</p>
      <p className="hover_data">Immersive, 1080p resolution screen brings the studio to you, HD display draws you in like a one-on-one session with the instructors. Feel the high energy of group workouts, real-time interaction with other riders and get instant feedback on your performance. </p>

    </div>
  </Fade>
  <Fade bottom>
    <div className="cycle_2 cycles_list">
      <img className="pc_cycles" src={two} alt=""/>
      <img className="mob_cycles" src={mob_two} alt=""/>

      <p className="image_para">Compact yet<br/> heavy duty</p>
      <p className="hover_data">With dimensions of LxB, the equipment will easily fit even in small places and convert them into your personal fitness studio. The design makes it quite easy to move the cycle around the house. Sturdy high-quality build quality ensures years of hassle-free operation. </p>


    </div>
  </Fade>
  <Fade bottom>
  <div className="cycle_3 cycles_list">
    <img className="pc_cycles" src={three} alt=""/>
    <img className="mob_cycles" src={mob_three} alt=""/>

    <p className="image_para">Connect to all<br/>your fitness devices</p>
    <p className="hover_data">Synq.fit connects to WiFi and Bluetooth, bringing all your devices together. Connect Synq.fit to your fitness wearable’s or even play your favorite music. Let all your connected devices push you to achieve your fitness goals.  </p>
  </div>
  </Fade>
  <Fade bottom>
    <div className="cycle_4 cycles_list">
      <img className="pc_cycles"  src={four} alt=""/>
      <img className="mob_cycles" src={mob_four} alt=""/>

      <p className="image_para">Belt Drive</p>
      <p className="hover_data">Bringing you the latest technology is exercise bikes - the belt drive. This makes the cycle extremely silent allowing you to have workout any time. With no noise from the cycle, immerse yourself in peaceful music and go on a long ride any time.</p>


    </div>
  </Fade>
  <Fade bottom>
    <div className="cycle_5 cycles_list">
      <img className="pc_cycles" src={five} alt=""/>
      <img className="mob_cycles" src={mob_five} alt=""/>

      <p className="image_para">Multi-Level <br/>Magnetic Resistance</p>
      <p className="hover_data">The Multi-Level Magnetic Resistance provides consistent resistance and enables the user to make accurate adjustments to the resistance levels. Zero friction point is the best part - it requires minimal maintenance.</p>


    </div>
  </Fade>
  <Fade bottom>
    <div className="cycle_6 cycles_list">
      <img  className="pc_cycles" src={six} alt=""/>
      <img className="mob_cycles" src={mob_six} alt=""/>

      <p className="image_para">Highly<br/>Adjustable</p>
      <p className="hover_data">The multipoint adjustment system allows you to change the position of the handlebar, the seat and even the screen. This makes the product useful for your entire family; motivating each one to take up exercise and achieve their unique fitness goals</p>


    </div>
  </Fade>

</div>

{/*cycle types over*/}

{/*ride everyday*/}
{/* <BackgroundImage
          Tag="div"
          className="ride_everyday"
          fluid={third_source}
  >


</BackgroundImage> */}

<div className="ride_everyday">
<Fade bottom cascade>
<div id="apps">
  <p className="head">
  Ride Every Day.<br/>
  Better Every Day.<br/>
  </p>

  {/* <p className='para'>
  Synq.Fit studio classes are just like going to the<br/>gym, except you get to stay at home. These<br/>classes with our trainers are meant to pump you <br/>up and push your own limits.
  </p> */}
  <p className='para'>
  Synq.Fit studio classes are just like going to the gym, except you get to stay at home. These classes with our trainers are meant to pump you up and push your own limits.
  </p>
  {/* {success ? <p className="para" style={{fontSize: '16px', color: '#111', padding: '20px 0'}}>{success}</p>: ''}
  {error ? <p className="para" style={{fontSize: '16px', color: 'red', padding: '20px 0'}}>{error}</p>: ''} */}
  <form className="form" style={{marginRight: '0', textAlign: 'center'}}>
        {/* <input className="browse_form" style={{backgroundColor: 'transparent !important'}}  onChange={e => {setInvite(e.target.value)}} name="invite_email" type="text"  placeholder="Get an invite"/> */}
        <button className="buy_bikes" type="submit"><Link  to="#contact" style={{color: '#fff', textDecoration: 'none'}}>Get Invite</Link></button>
    </form>
  <button class="contact_us" style={{display: "none"}}><Link  style={{textDecoration:'none',color:'#dbdfeb'}}to="/product/">Pre-Book Now</Link></button>
  </div>
</Fade>
  <Fade right>
     <img src={ride_img} className="ride_img" />
  </Fade>
  <div>
    
  </div>
</div>
{/* ride everyday over */}


{/* features */}
<div className="features">
<h1>
  Features
</h1>
<div className="feature_list">
  <Fade bottom cascade >
    <div className="item_1">
      <div className="feature_list_img">
        <img className="pc_feature" src={ic_calender}/>
        <img className="mob_feature" src={mob_ic_calender}/>
        </div>
      <p className="head">Live Sessions Everyday</p>
      <p className="para">Feel the energy of a live workouts, interact real-time with the trainers and get instant feedback.</p>
    </div>
  </Fade>
  <Fade bottom cascade style={{transitionDelay: '500ms'}}>
    <div className="item_2">
      <div className="feature_list_img">
        <img className="pc_feature" src={ic_home}/>
        <img className="mob_feature" src={mob_ic_home}/>

      </div>

      <p className="head">Gym for the Entire Family</p>
      <p className="para">Be it your mother, wife, brother, son  or father - create and track individual profiles and data with Synq.Fit bike.</p>
    </div>
  </Fade>

  <Fade bottom cascade style={{transitionDelay: '1000ms'}}>
    <div className="item_3">
      <div className="feature_list_img">
        <img className="pc_feature" src={ic_video}/>
        <img className="mob_feature" src={mob_ic_video}/>

      </div>

      <p className="head">On-demand Video Library</p>
      <p className="para">In the mood for a quick 5-mins session with an instructor or a 90-mins session on a scenic mountain? Our library has it all.</p>
    
    </div>
  </Fade>
  <Fade bottom cascade >
    <div className="item_4">
      <div className="feature_list_img">
        <img className="pc_feature" src={ic_cycle}/>
        <img className="mob_feature" src={mob_ic_cycle}/>

      </div>
      <p className="head">Join the Movement</p>
      <p className="para">Connect with friends and riders from across the world, go at your own pace, compete at different levels.</p>
    
    </div>
  </Fade>
  <Fade bottom cascade style={{transitionDelay: '500ms'}}>
    <div className="item_5">
      <div className="feature_list_img">
        <img className="pc_feature" src={ic_trainers}/>
        <img className="mob_feature" src={mob_ic_trainers}/>

      </div>
      
      <p className="head">Best-in-class Instructors</p>
      <p className="para">Our motivating instructors ensure that you keep coming back for more of what you like and experience new ones.</p>
    
    </div>
  </Fade>
  <Fade bottom style={{transitionDelay: '1000ms'}}>
  <div className="item_6">
    <div className="feature_list_img">
      <img className="pc_feature"  src={ic_diverse}/>
      <img className="mob_feature" src={mob_ic_diverse}/>

    </div>
    <p className="head">Diverse Class Types</p>
    <p className="para">Classes ranging from energizing HIIT training sessions to stress-busting easy rides with hand-picked music to boast.</p>
  
  </div>
  </Fade>

</div>
</div>
{/* features over*/}




{/*trainers*/}


<div  className="ride_everyday1" style={{height: 'auto', justifyContent: 'space-evenly' }}>
 
  <Fade left>
    <div>
      <img src={Trainers} />
    </div>
  </Fade>

  <Fade bottom cascade>
  <div>
  <p className="head">
  Meet the trainers
  </p>

  <p className='para'>
  Synq.Fit studio classes are just like going to the gym, except you get to stay at home. These classes with our trainers are meant to pump you up and push your own limits.
  </p>

  {/* <button class="contact_us"><Link  style={{textDecoration:'none',color:'#dbdfeb'}}to="/trainers/">Trainers Profile</Link></button> */}
  </div>
  </Fade>
  
  

</div>
{/* trainers over */}

<div className="upcoming_video">
  {/* <h3 className="sub_heading">Lorem ipsum, Live and Upcoming WORKOUTS</h3> */}
  <h1 className="heading">Live and Upcoming WORKOUTS</h1>
  
   {/* Video1 */}
 
  <Dialog
    maxWidth="lg"
    onClose={handleVideoClose}
    style={{marginTop: '110px'}}
    aria-labelledby="customized-dialog-title"
    open={isOpen}
  >
    <IconButton style={{color: '#fff'}} aria-label="close" onClick={handleVideoClose}>
      <CloseIcon />
    </IconButton>
    <Player
      playsInline
      poster="https://synq-fit.s3.ap-south-1.amazonaws.com/media/class/thumbnail/STRENGTH-AND-CARDIO-RIDE-2-VARUN-ON-71020.00_04_01_08.Still001_duzluWH.jpg"
      src="https://synq-fit.s3.ap-south-1.amazonaws.com/media/class/upload/1620915130_STRENGTH.mp4"
    />
  </Dialog>

  {/* video 2 */}

  <Dialog
    maxWidth="lg"
    onClose={handleVideoClose2}
    style={{marginTop: '110px'}}
    aria-labelledby="customized-dialog-title"
    open={isOpen2}
  >
    <IconButton style={{color: '#fff'}} aria-label="close" onClick={handleVideoClose2}>
      <CloseIcon />
    </IconButton>
    <Player
      playsInline
      poster="https://synq-fit.s3.ap-south-1.amazonaws.com/media/class/thumbnail/LOW-IMPACT-RIDE-5-GAGAN.00_07_43_03.Still001.jpg"
      src="https://synq-fit.s3.ap-south-1.amazonaws.com/media/class/upload/1312616321_Low%2520Impact%2520Ride.mp4"
    />
  </Dialog>

  {/* video3 */}

  <Dialog
    maxWidth="lg"
    onClose={handleVideoClose3}
    style={{marginTop: '110px'}}
    aria-labelledby="customized-dialog-title"
    open={isOpen3}
  >
    <IconButton style={{color: '#fff'}} aria-label="close" onClick={handleVideoClose3}>
      <CloseIcon />
    </IconButton>
    <Player
      playsInline
      poster="https://synq-fit.s3.ap-south-1.amazonaws.com/media/class/thumbnail/pyramid_thumbnail.jpg"
      src="https://synq-fit.s3.ap-south-1.amazonaws.com/media/class/upload/1512706394_PYRAMID%2520RIDE%25201.mp4"
    />
  </Dialog>

  {/* video4 */}

  <Dialog
    maxWidth="lg"
    onClose={handleVideoClose4}
    style={{marginTop: '110px'}}
    aria-labelledby="customized-dialog-title"
    open={isOpen4}
  >
    <IconButton style={{color: '#fff'}} aria-label="close" onClick={handleVideoClose4}>
      <CloseIcon />
    </IconButton>
    <Player
      playsInline
      poster="https://synq-fit.s3.ap-south-1.amazonaws.com/media/class/thumbnail/PSD.jpg"
      src="https://synq-fit.s3.ap-south-1.amazonaws.com/media/class/upload/1619890141_TABATA%2520.mp4"
    />
  </Dialog>
  <div>
  <Grid container className="grid">
      <Grid item xs={12} sm={4}>
       <Fade bottom>
        <Card className="root">
          <CardMedia
            className="cover"
            image="https://synq-fit.s3.ap-south-1.amazonaws.com/media/class/thumbnail/STRENGTH-AND-CARDIO-RIDE-2-VARUN-ON-71020.00_04_01_08.Still001_duzluWH.jpg"
            title="Live from space album cover"
          />
          <div className="details">
            <CardContent className="content">
            <Typography variant="subtitle1" color="textSecondary" className="cat">
                Category
              </Typography>
              <Typography component="h5" variant="h5" className="title" >
                Strength + Cardio
              </Typography>
              <Typography variant="subtitle1" color="textSecondary" className="desc">
                Difficult <span style={{margin: '0 10px'}}>|</span> 21 Mins
              </Typography>
            </CardContent>
            <div className="controls">
              <img  onClick={() => handleVideoOpen()} style={{cursor: "pointer"}} src={join_btn}/>
            </div>
          </div>
        
        </Card>
       </Fade>
      </Grid>
      <Grid item xs={12} sm={4}>
        <Fade bottom >
          <Card className="root">
            <CardMedia
              className="cover"
              image="https://synq-fit.s3.ap-south-1.amazonaws.com/media/class/thumbnail/LOW-IMPACT-RIDE-5-GAGAN.00_07_43_03.Still001.jpg"
              title="Live from space album cover"
            />
            <div className="details">
              <CardContent className="content">
                <Typography variant="subtitle1" color="textSecondary" className="cat">
                  Category
                </Typography>
                <Typography component="h5" variant="h5" className="title"  >
                  Low Impact Ride
                </Typography>
                <Typography variant="subtitle1" color="textSecondary" className="desc">
                  Easy <span style={{margin: '0 10px'}}>|</span> 17 Mins
                </Typography>
              </CardContent>
              <div className="controls">
                <img  onClick={() => handleVideoOpen2()} style={{cursor: "pointer"}} src={join_btn}/>
              </div>
            </div>
          
          </Card>
        </Fade>
      </Grid>
    </Grid>


    <Grid container className="grid">
      <Grid item xs={12} sm={4}>
        <Fade bottom style={{transitionDelay: '500ms'}}>
          <Card className="root">
            <CardMedia
              className="cover"
              image="https://synq-fit.s3.ap-south-1.amazonaws.com/media/class/thumbnail/pyramid_thumbnail.jpg"
              title="Live from space album cover"
            />
            <div className="details">
              <CardContent className="content">
              <Typography variant="subtitle1" color="textSecondary" className="cat">
                  Category
                </Typography>
                <Typography component="h5" variant="h5" className="title"  >
                  Pyramid Ride
                </Typography>
                <Typography variant="subtitle1" color="textSecondary" className="desc">
                  Medium <span style={{margin: '0 10px'}}>|</span> 20 Mins
                </Typography>
              </CardContent>
              <div className="controls">
                <img  onClick={() => handleVideoOpen3()} style={{cursor: "pointer"}} src={join_btn}/>
              </div>
            </div>
          
          </Card>
        </Fade>
      </Grid>
      <Grid item xs={12} sm={4}>
        <Fade bottom style={{transitionDelay: '500ms'}}>
          <Card className="root">
            <CardMedia
              className="cover"
              image="https://synq-fit.s3.ap-south-1.amazonaws.com/media/class/thumbnail/PSD.jpg"
              title="Live from space album cover"
            />
            <div className="details">
              <CardContent className="content">
                <Typography variant="subtitle1" color="textSecondary" className="cat">
                  Category
                </Typography>
                <Typography component="h5" variant="h5" className="title" >
                  TABATA RIDE
                </Typography>
                <Typography variant="subtitle1" color="textSecondary" className="desc">
                  Difficult <span style={{margin: '0 10px'}}>|</span> 21 Mins
                </Typography>
              </CardContent>
              <div className="controls">
                <img  onClick={() => handleVideoOpen4()} style={{cursor: "pointer"}} src={join_btn}/>
              </div>
            </div>
          
          </Card>
        </Fade>
      </Grid>
    </Grid>
  </div>
</div>


 
{/* testimonial */}
{/* <div className="testimonial">
  <h1 className="head">TESTIMONIALS</h1>
  {/* <p className="para">
  "It's just amazing. SynqFit is both attractive and<br/>highly adaptable.I have gotten at least 50 times<br/>the value from SynqFit. If you want real<br/>marketing that works and effective<br/>implementation - SynqFit's got you covered."
  </p> 
    <p className="para">
  "It's just amazing. SynqFit is both attractive and highly adaptable.I have gotten at least 50 times the value from SynqFit. If you want real marketing that works and effective implementation - SynqFit's got you covered."
  </p>

  <p className="para_name">- Garcia X</p>

</div> */}
{/* testimonial over*/}


{/* community feature */}

<div className="comm_feature" id="community">
  <h1 className="heading">Community Features</h1>
  <div className="card_features">
<Fade bottom >
<div className="card">
  <div className="card_img">
  <img src={card_one} alt="" className="feature_img"/>
  </div>
  <p className="card_head">Ride with Friends</p>
  <p className="card_para">Connect with your friends on Synq.Fit and keep the spirits high.</p>
</div>
</Fade>

<Fade bottom style={{transitionDelay: '500ms'}}>
<div className="card">
<div className="card_img">
  <img src={card_two} alt="" className="feature_img"/>
</div>
  <p className="card_head">Compete with Strangers</p>
  <p className="card_para">Select a partner who has a profile <br/>similar to yours in terms of age,<br/> weight, gender and difficulty level.<br/> When it comes to testing your strength and stamina,
<br/>it’s GAME ON.</p>
</div>
</Fade>

<Fade bottom style={{transitionDelay: '1000ms'}}>
<div className="card">
<div className="card_img">
  <img src={card_three} alt="" className="feature_img"/>
</div>
  <p className="card_head">Get mesmerized</p>
  <p className="card_para">Go on long rides with scenic views<br/> and pleasant music that will help<br/> you unwind.</p>
</div>


</Fade>
</div>


    
    {/* <form className="form"  onSubmit={onInviteSubmit} style={{ textAlign: 'center', margin: '28px 0' }}>
        <input className="browse_form"  type="text" onChange={e => {setInvite(e.target.value)}} name="invite_email" placeholder="Get an invite"/>
        <button className="buy_bikes" type="submit" style={{marginRight: 'auto'}}>Invite</button>
    </form> */}
</div>

{/* News logo */}

<Box className="news_logo_box">
    <h1 className="news_head">IN THE NEWS</h1>
    <div className="desktop_slider"> 
    <Carousel>
         <Carousel.Item>
            <Grid container style={{justifyContent: 'space-evenly'}}> 
              <Grid item >
                  <Box className="news_box">
                      <img src={news1} style={{width: '195px'}}/>
                  </Box> 
              </Grid>
              <Grid item>
                  <Box className="news_box">
                      <img src={news2} style={{width: '281px'}}/>
                  </Box>
              </Grid>
              <Grid item>
                <Box className="news_box">
                      <img src={news3} style={{width: '281px'}}/>
                  </Box>
              </Grid>
              <Grid item>
                <Box className="news_box">
                      <img src={news4} style={{width: '281px'}}/>
                  </Box>
              </Grid>
            </Grid>
         </Carousel.Item>
         <Carousel.Item>
          <Grid container style={{justifyContent: 'space-evenly'}}>
          <Grid item >
                <Box className="news_box">
                    <img src={news5} style={{width: '195px'}}/>
                </Box>
            </Grid>
            <Grid item>
                <Box className="news_box">
                    <img src={news1} style={{width: '281px'}}/>
                </Box>
            </Grid>
            <Grid item>
              <Box className="news_box">
                    <img src={news2} style={{width: '281px'}}/>
                </Box>
            </Grid>
            <Grid item>
              <Box className="news_box">
                    <img src={news3} style={{width: '281px'}}/>
                </Box>
            </Grid> 
          </Grid>
         </Carousel.Item>
       </Carousel>
    </div>
    <div className="mobile_slider">
    <Carousel>
         <Carousel.Item>
            <Grid container style={{justifyContent: 'space-evenly'}}> 
              <Grid item >
                  <Box className="news_box">
                      <img src={news1} style={{width: '195px'}}/>
                  </Box> 
              </Grid>
              
            </Grid>
         </Carousel.Item>
         <Carousel.Item>
            <Grid container style={{justifyContent: 'space-evenly'}}> 
              <Grid item >
                  <Box className="news_box">
                      <img src={news2} style={{width: '195px'}}/>
                  </Box> 
              </Grid>
              
            </Grid>
         </Carousel.Item>
         <Carousel.Item>
            <Grid container style={{justifyContent: 'space-evenly'}}> 
              <Grid item >
                  <Box className="news_box">
                      <img src={news3} style={{width: '195px'}}/>
                  </Box> 
              </Grid>
              
            </Grid>
         </Carousel.Item>
         <Carousel.Item>
          <Grid container style={{justifyContent: 'space-evenly'}}>
            <Grid item >
                <Box className="news_box">
                    <img src={news4} style={{width: '195px'}}/>
                </Box>
            </Grid>
           
          </Grid>
         </Carousel.Item>
         <Carousel.Item>
          <Grid container style={{justifyContent: 'space-evenly'}}>
            <Grid item >
                <Box className="news_box">
                    <img src={news5} style={{width: '195px'}}/>
                </Box>
            </Grid>
           
          </Grid>
         </Carousel.Item>
       </Carousel>
    </div>
</Box>

{/* <form className="form" onSubmit={onInviteSubmit} style={{ textAlign: 'center', marginTop: '-10px', margin: '0', backgroundColor: '#181C2B' }}>
{success ? <p className="form_paragraph" style={{fontSize: '16px', padding: '20px 0', color: '#fff'}}>{success}</p>: ''}
{error ? <p className="form_paragraph" style={{fontSize: '16px', color: 'red', padding: '20px 0'}}>{error}</p>: ''}
        <input className="browse_form" style={{backgroundColor: 'transparent !important'}}  onChange={e => {setInvite(e.target.value)}} name="invite_email" type="text"  placeholder="Get an invite"/>
        <button className="buy_bikes" style={{marginRight: 'auto'}}>Get Invite</button>
    </form> */}

{/* community feature over*/}

{/*fit everyday */}
{/* <BackgroundImage
          Tag="div"
          className="fit_everyday"
          fluid={fourth_source}>

<h1 className="fit_heading">Better Everyday</h1>
</BackgroundImage> */}
{/*fit everyday over*/}
 

{/*Contact Us*/}
<div className="contact_us_form" >
  <div className="contact_text">
    <p className="form_heading">Contact Us</p>
    <p className="form_paragraph">Feel free to reach out to us and get to<br/> know more about Synq.Fit.
      <br/><br/>Join the synq community now!</p>  
       
      <p className="form_label">EMAIL</p>
      <p className="form_value">info@synq.fit</p>
      <p className="form_label">PHONE NUMBER</p>
      <p className="form_value">+91-8700128721</p> 
      <span className="social_icons">
        <a href="https://www.linkedin.com/company/synqfit/" target="_blank">
          <img src={linkedin}/>
        </a>
        <a href="https://www.instagram.com/synq.fit/" target="_blank">
          <img src={insta}/>
        </a>
        <a href=" https://www.facebook.com/Synqfit-111459097451071" target="_blank">
          <img src={fb}/>
        </a>
      </span> 
  

      <p className="form_copyright">© Synq.Fit 2020</p>
      <p className="mobile_footer_content">Or Leave your contact details to get the invite.</p>
       
  </div>

  <div className="form_contact_us" id="contact"><div className="form_box">
  <h6 className="form_heading"  >Get an Invite</h6>
     {/* {error ? <p className="plan_texts" style={{fontSize: '16px', paddingTop: '200px', color: 'red', padding: '20px 0'}}>{error}</p>: ''} */}
    <form onSubmit={handleSubmit(formsubmit)}>
    {success  
        ? 
        <span className="plan_texts" style={{fontSize: '16px', paddingTop: '200px', padding: '20px 0'}}>{success}</span>
        : 
        error
        ?
        <p className="plan_texts" style={{fontSize: '16px', paddingTop: '200px', color: 'red', padding: '20px 0'}}>{error}</p>
        :
        ''
      }
    <input 
        required 
        name="username" 
        value={formdata.username} 
        onChange={formData} 
        className="form_input" 
        placeholder="Name"
        ref={register({
          required: "This input is required.",

        
        })}
      />
       <ErrorMessage
        errors={errors}
        name="username"
        render={({ messages }) => {
          console.log("messages", messages);
          return messages 
            ? Object.entries(messages).map(([type, message]) => (
                <p  key={type}>{message}</p>
              ))
            : null;
        }}
      />
    <input 
        required 
        name="email" 
        value={formdata.email} 
        onChange={formData} 
        type="email"  
        className="form_input" 
        placeholder="Email"
        ref={register({
          required: "This input is required.",
          pattern: {
            value: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/,
            message: "Please enter a valid email. example: abc@example.com"
          },
        
        })}
      />
      <ErrorMessage
        errors={errors}
        name="email"
        render={({ messages }) => {
          console.log("messages", messages);
          return messages 
            ? Object.entries(messages).map(([type, message]) => (
                <p  key={type}>{message}</p>
              ))
            : null;
        }}
      />
    <PhoneInput
      className="form_phone_input"
      defaultCountry="IN"
      value={value}
      onChange={setValue}
      placeholder="Phone Number"
      error={value ? (isValidPhoneNumber(value) ? undefined : 'Invalid phone number') : 'Phone number required'} 
      
    />
    {/* <input required name="phone" value={formdata.phone} onChange={formData}  className="form_input" placeholder=" Mobile number"/> */}
    <input 
      required 
      name="city"  
      onChange={formData} 
      value={formdata.city} 
      className="form_input" 
      placeholder="City"
      ref={register({
        required: "This input is required.",

      
      })}
    />
     <ErrorMessage
      errors={errors}
      name="city"
      render={({ messages }) => {
        console.log("messages", messages);
        return messages 
          ? Object.entries(messages).map(([type, message]) => (
              <p  key={type}>{message}</p>
            ))
          : null;
      }}
    />
  
    <textarea 
      required 
      name="comment"  
      value={formdata.comment} 
      onChange={formData} 
      className="form_text_area" 
      placeholder="Message" 
      rows="10" 
      cols="10"
      ref={register({
        required: "This input is required.",

      
      })}
    />
     <ErrorMessage
      errors={errors}
      name="city"
      render={({ messages }) => {
        console.log("messages", messages);
        return messages 
          ? Object.entries(messages).map(([type, message]) => (
              <p  key={type}>{message}</p>
            ))
          : null;
      }}
    />

    <button className="form_button">Submit</button>
    </form>
    </div>
  </div>
  
{/*Contact Us over*/}

{/* 
<div className="phonecode">
      <div className="code">
    <PhoneCode
        onSelect={code => console.log(code)} // required
        showFirst={['US', 'IN']}
        defaultValue='select county'
        id='some-id'
        name='some-name'
        className='some class name'
        optionClassName='some option class name'
    />
    </div>
    <div>
    <input required name="phone" className="mobile_input" placeholder="Mobile number"/>
    </div>
    </div> */}
    </div>
 


 </Layout>
)
}

export default HomePage

export const pageQuery = graphql`
query MyQuery2 {
  __typename
  image_landing:file(relativePath: {eq: "images/image-landing.png"}) {
    childImageSharp {
      fluid(fit: COVER) {
        aspectRatio
        base64
        srcWebp
        src
      }
    }
  }

  image_landing_mini: file(relativePath: {eq: "mob_images/image-landing-mobile.png"}) {
    id
    childImageSharp {
      fluid(maxWidth: 450) {
        src
      }
    }
  }



  bike_hero:file(relativePath: {eq: "images/bike-hero-image.png"}) {
    childImageSharp {
      fluid(fit: COVER) {
        aspectRatio
        base64
        srcWebp
        src
      }
    }
  }

  image_app:file(relativePath: {eq: "images/image-app.png"}) {
    childImageSharp {
      fluid(fit: COVER) {
        aspectRatio
        base64
        srcWebp
        src
      }
    }
  }


  image_app_mini: file(relativePath: {eq: "mob_images/image-app-mobile.png"}) {
    id
    childImageSharp {
      fluid(maxWidth: 450) {
        src
      }
    }
  }

  better_everyday:file(relativePath: {eq: "images/better_everyday.png"}) {
    childImageSharp {
      fluid(fit: COVER) {
        aspectRatio
        base64
        srcWebp
        src
      }
    }
  }


  better_everyday_mini: file(relativePath: {eq: "mob_images/mobile_better.png"}) {
    id
    childImageSharp {
      fluid(maxWidth: 450) {
        src
      }
    }
  }

 
}
`