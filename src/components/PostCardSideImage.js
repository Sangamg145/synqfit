import React from 'react'
import { Link } from 'gatsby'

import Image from './Image'
import './PostCard.css'

const PostCardSideImage = ({
  featuredImage,
  title,
  slug,
  className = '',
  ...props
}) => (
 <div>  
{props.post.map(data=>
  <Link to={data.node.fields.slug} className={`PostCard ${className}`}>
    {data.node.frontmatter.featuredImage && (
      <div className="PostCard--Image relative">
        <Image background src={data.node.frontmatter.featuredImage} alt={data.node.frontmatter.title} />
      </div>
    )}
    <div className="PostCard--Content">
    {data.node.frontmatter.title && <h3 className="PostCard--Title">{data.node.frontmatter.title}</h3>}
    {data.node.frontmatter.date && <p className="PostCard--Title" style={{fontSize: '14px'}}>Posted on {data.node.frontmatter.date}</p>}

    </div>
    {console.log(props.post)}
  </Link>)}
  </div>
)

export default PostCardSideImage
