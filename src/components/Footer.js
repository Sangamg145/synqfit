import React,{useState} from 'react'
// import InstagramFeed from './InstagramFeed'
import './Footer.css'

import insta from "../assets/images/instagram.png"
import fb from "../assets/images/facebook.png"
import tweet from "../assets/images/twitter.png"
import apple from "../assets/images/apple_store.png"
import android from "../assets/images/android_store.png"
import { Link } from 'gatsby'
import axios from "axios"
import Spin from "./Spinner"
import Noti from "./Notification"
export default  () => 

{
const [email_id,setEmail]=useState('');
const [loaded,setLoaded]=useState(null);
const [success, setSuccess] = useState(null)
const [error, setError] = useState(null)

const myemail = (e)=>{
  setEmail(e.target.value)
}

const notification=(event)=>{
event.preventDefault();
setLoaded(false);
// const email=event.target.elements.notification_input.value;
const data = {
  "email":email_id,
  "leads_type": 2
};
axios.post('https://admin.synq.fit/core/api/leads/',data).then(response=>{
  console.log(response);
  setLoaded(true);
  setEmail('')
  setSuccess('Thank you for newsletter')
}).catch(err=>{
  setLoaded(true);
  setEmail('')
  setError('something went wrong')


})
}


 
return(
 
<div className="footer">
  {!loaded&&loaded!=null&&<Spin/>}
  {loaded&&<Noti>Email succesfully registered</Noti>}
<div className="footer_menu">
  <div className="footer_menu_1">
    
    <div>
      <p className="footer_heading">Synq.fit bike</p>
      <ul>
      <li><Link to="/product/">Buy the Bike</Link></li>
        {/* <li>Technical Details</li>
        <li>Product Review & Testimonial</li>
        <li>Synq.fit Plans</li> */}

      </ul>
    </div>
    
    <div>      
      <p className="footer_heading">About</p>
      <ul>
        <li><Link to="/about">About Us</Link></li>
        {/* <li>Trainers</li> */}
        {/* <li>Team</li> */}
        <li><Link to="/blog/">Blog</Link></li>



      </ul>
    </div>
    
    <div>      
      <p className="footer_heading">Support</p>
      <ul>
      <li><Link to="/contact/">Contact Us</Link></li>
        <li><Link to="/faq/">FAQs</Link></li>
        {/* <li>Bike Warranty</li> */}
        <li><Link to="/policy/">Privacy Policy</Link></li> 
        <li><Link to="/policy/">Terms and Conditions</Link></li>

        
      </ul>
    </div>
        
    
  </div>

  <div>
  <p className="footer_heading right_footer">Sign up to receive product and services notifications</p>
      {success ? <p className="footer_heading right_footer" style={{fontSize: '16px', padding: '20px 0'}}>{success}</p>: ''}
      {error? <p className="footer_heading right_footer" style={{fontSize: '16px', padding: '20px 0', color: 'red'}}>{error}</p>: ''}
  <form onSubmit={notification}>
  <input name="notification_input" onChange={myemail} value={email_id} required className="footer_input" type="email"/>
  <button className="footer_form_button">Submit</button> 
  </form>
{/* 
<p className="footer_heading right_footer">
Download our App
</p>
<img className="mobile_img" src={apple} alt="apple"/>
<img className="mobile_img" src={android} alt="android" /> */}

  </div>
  

</div>
<div >
  <div >
    <div className="footer_menu_1" style={{marginTop: '70px', marginLeft: '60px'}}>
   <div style={{width: '70%'}}>
   <p className="footer_business_heading">
      Business ID: 727195751568629 <br /><br />
      Legal name: DIY FITNESS PRIVATE LIMITED<br /><br />
      Address: 9TH FLOOR, TOWER -B SAS TOWER MEDANTA <br /><br />THE MEDICITY COMPLEX SECTOR-38, Gurgaon, 122001, IN<br /><br />
      Phone number: +91 99711 27387<br /><br />
      Email address: info@synq.fit<br /><br />
      Website: https://synq.fit/<br /><br />
      Country: IN
    </p>
   </div>
    </div>

  </div>

</div>


{/* 
<div className="footer_text">
  <h2>About Us</h2>
  <span >
  One Stop Solution for your workout from home<br/>
It is a digital world and fitness is not behind. In this connected era, it has become essential to combine physical and digital worlds, and Synq.Fit merges these seamlessly for you. We are a new age connected fitness company that keeps you engaged in instructor-led fitness activities. Keep laziness and stress at a bay and leave it to Synq.Fit to solve all your fitness needs with our unique curated program.<br/>
Synq.Fit helps you to maintain these fitness levels by bringing in best in class workout programs at your home. With live sessions every day, Synq.Fit coverts your living room into an energetic fitness studio motivating you to sweat it out. It is essential to maintain a basic fitness level to improve work efficiency and productivity, and Synq.Fit is an ultimate solution to all your cardio issues. <br/>
Bringing in world class technology, Synq.Fit helps you achieve your fitness goals from the comfort of your living room. The membership plans are especially curated to serve all your needs. <br/>
In the last few years, work from home has gained momentum in India. The purpose of this shift was to keep individuals motivated and improve their work life balance, but this nature of work also blurred the lines between personal and professional lives. While work from home is considered a good option, it can lead to many fitness issues in individuals. Lack of physical activity can lead to obesity, type 2 diabetes, heart disease and even cancer.<br/>
Synq.Fit also improves your work life balance by managing your social as well as physical fitness appetite. The program not only adds fun into fitness, it also helps you maintain a good social circle by interacting with friends and family.<br/>
Synq.fit is not just home cardio solution, but a fitness movement. Our users not just stay healthy and fit, but also become a part of the bigger riding movement that aims to make India healthier. <br/>
</span>
</div> */}

</div>
)
}
