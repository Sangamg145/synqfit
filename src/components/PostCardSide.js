import React from 'react'
import { Link } from 'gatsby'

import Image from './Image'
import './PostCard.css'

const PostCardSide = ({
  featuredImage,
  title,
  excerpt,
  slug,
  categories = [],
  className = '',
  ...props
}) => (
  <Link to={slug} className={`PostCard ${className}`}>
    
    <div className="PostCard--Content">
    {title && <h3 className="PostCard--Title">{title}</h3>}
    {excerpt && <div className="PostCard--Excerpt"><p style={{fontSize: '14px'}}>{excerpt}</p></div>}
    </div>
  </Link>
)

export default PostCardSide
