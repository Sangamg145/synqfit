import React,{useState} from 'react'
import { Link } from 'gatsby'
import axios from "axios"
import styles from "./Notification.module.css"
import Spin from "./Spinner"
const Notification = ({children}) =>
{
const [hide,setHide]=useState(false);

return(
    <div className={`${hide&&styles.hidden} ${styles.alert}`}>
    <span onClick={()=>setHide(!hide)} className={`${hide&&styles.hidden} ${styles.closebtn}`}>&times;</span>
   {children}
  </div>
    )
    
}
export default Notification;


