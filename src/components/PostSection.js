import React from 'react'

import PostCard from '../components/PostCard'
import PostCardSide from '../components/PostCardSide'
import Newsletter from '../components/Newsletter'

import './PostSection.css'

class PostSection extends React.Component {
  static defaultProps = {
    posts: [],
    title: '',
    limit: 3,
    showLoadMore: true,
    loadMoreTitle: 'Load More',
    perPageLimit: 3
  }

  state = {
    limit: this.props.limit
  }

  increaseLimit = () =>
    this.setState(prevState => ({
      limit: prevState.limit + this.props.perPageLimit
    }))

  render() {
    const { posts, title, showLoadMore, loadMoreTitle } = this.props,
      { limit } = this.state,
      visiblePosts = posts.slice(0, limit || posts.length)

      const visiblePostsSide = posts.slice(0, 2 || posts.length)


    return (
      <div className="PostSection">
        <div className="first">
        {title && <h2 className="PostSection--Title">{title}</h2>}
        {!!visiblePosts.length && (
          <div>
            {visiblePosts.map((post, index) => (
              <PostCard key={post.title + index} {...post} />
            ))}
          </div>
        )}
        {showLoadMore && visiblePosts.length < posts.length && (
          <div className="taCenter">
            <button className="button" onClick={this.increaseLimit}>
              {loadMoreTitle}
            </button>
          </div>
        )}
      </div>
      <div className="side_bar_post">
      {!!visiblePostsSide.length && (
          <div>
          <p style={{paddingLeft:'2rem', fontSize: '18px', fontWeight: 'bold'}}>Recent posts</p>

            {visiblePostsSide.map((post, index) => (
              <PostCardSide key={post.title + index} {...post} />
            ))}


          </div>
        )}


    {/* <div className="newsletter">
        <p className="newsletter_heading">Join our newsletter</p>
        <p className="newsletter_para">Get exclusive weekly deals with our newsletter subscription</p>
        <input className="newsletter_input" placeholder="Email address"/>
        <button className="newsletter_btn">Join Newsletter</button>


      </div> */}
      <Newsletter/>

      </div>


      
      </div>
    )
  }
}

export default PostSection
