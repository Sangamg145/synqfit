import React from 'react'
import { Link } from 'gatsby'


const SinglePostCategory = ({ category }) => (
  <div className="PostCategoriesNav">
   {console.log(category)}
   <ul style={{listStyle:'none'}}>
    {category.edges.map((category, index) => (
     <Link
     exact="true"
     className="NavLink"
     key={category.node.fields.slug + index}
     to={category.node.fields.slug}
   >
     <li key={index} style={{fontSize: '16px'}}>{category.node.frontmatter.title}</li>
   </Link>
 ))} 
 </ul>
 </div>
    )
    
    //  <Link
    //     exact="true"
    //     className="NavLink"
    //     key={category.node.fields.slug + index}
    //     to={category.node.fields.slug}
    //   >
    //     {category.node.frontmatter.title}
    //   </Link>
    // ))} 

   
//   </div>


export default SinglePostCategory
