import React,{useState} from 'react'
import { Link } from 'gatsby'
import axios from "axios"
import Spin from "./Spinner"
const Newsletter = () =>
{
const [email_id,setEmail] = useState('');
const [loaded,setLoaded]=useState(true);
const notification=(event)=>{
    event.preventDefault();
    setLoaded(false);
    const data = {
      email:email_id
    };
    axios.post('http://15.206.233.88/core/api/newsletter/',data).then(response=>{
      console.log(response);
      setLoaded(true);
      setEmail('');
    }).catch(err=>{
        setLoaded(true);
        setEmail('');
    })
    }

const myemail = (e)=>{
  setEmail(e.target.value)
}

return(
  
<div className="newsletter">
    {!loaded&&<Spin/>}
<p className="newsletter_heading">Join our newsletter</p>
<p className="newsletter_para">Get exclusive weekly deals with our newsletter subscription</p>
<form onSubmit={notification}>
<input required type="email" onChange={myemail} value={email_id} name="notification_input" className="newsletter_input" placeholder="Email address"/>
<button className="newsletter_btn">Join Newsletter</button>
</form>

</div>
    )
    
}
export default Newsletter;


