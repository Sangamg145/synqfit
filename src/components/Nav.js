import React, { Component } from 'react'
import { Location } from '@reach/router'
import { Link } from 'gatsby'
import { Menu, X } from 'react-feather'
import Logo from './Logo'
import logo from "../assets/images/logo.png"

import burg from "../assets/images/burg.png"
import insta from "../assets/images/instagram.png"
import fb from "../assets/images/facebook.png"
import tweet from "../assets/images/twitter.png"
import apple from "../assets/images/apple_store.png"
import android from "../assets/images/android_store.png"



// mobile Content
import mob_logo from "../assets/mob_images/logo-compact.png"
import mob_burg from "../assets/mob_images/mobile_burg.png"


import './Nav.css'

export class Navigation extends Component {
  state = {
    active: false,
    activeSubNav: false,
    currentPath: false
  }

  componentDidMount = () =>
    this.setState({ currentPath: this.props.location.pathname })

  handleMenuToggle = () => this.setState({ active: !this.state.active })

  // Only close nav if it is open
  handleLinkClick = () => this.state.active && this.handleMenuToggle()

  toggleSubNav = subNav =>
    this.setState({
      activeSubNav: this.state.activeSubNav === subNav ? false : subNav
    })

  render() {
    const { active } = this.state,
      { subNav } = this.props,
      NavLink = ({ to, className, children, ...props }) => (
        <Link
          to={to}
          className={`NavLink ${
            to === this.state.currentPath ? 'active' : ''
          } ${className}`}
          onClick={this.handleLinkClick}
          {...props}
        >
          {children}
        </Link>
      )

    return (
      <>
      <div className="header">
      <div className='logo'>
      <Link to="/"><div className='logo'>
      <img src={logo} alt="logo" />
      </div></Link>      </div>
      <div className="navheader">
       <ul>
       <li><Link to="/#bike">BIKE</Link></li>
       <li><Link to="/#apps">APP</Link></li>
       <li><Link to="/#community">COMMUNITY</Link></li>



       </ul>
      </div>
{/*       
      <div className="hamburg">
      <img src={burg} alt="burger" />

      </div>
       */}
   
    </div>

    {/* mobile menu */}
    <div className="header_mobile">
      <div className='mobile_logo'>
      <Link to="/"><div className='logo'>
      <img src={mob_logo} alt="logo" />
      </div></Link>
      </div>
      
      
      <div className="mobile_hamburg">
      <img src={mob_burg} alt="burger" />

      </div>
      
   
    </div>
    </>
    )
  }
}

export default ({ subNav }) => (
  <Location>{route => <Navigation subNav={subNav} {...route} />}</Location>
)
