import React from 'react'
import { Link } from 'gatsby'

import Image from './Image'
import './PostCard.css'
import BackgroundImage from 'gatsby-background-image'

const PostCard = ({
  featuredImage,
  title,
  excerpt,
  slug,
  categories = [],
  className = '',
  ...props
}) => (
 <>
 <div className="" style={{backgroundImage:` linear-gradient(
  rgba(0, 0, 0, 0.5),
  rgba(0, 0, 0, 0.5)
),url(${featuredImage})`, margin:'5% auto',backgroundSize:'cover',color:'white',filter:'drop-shadow(2px 4px 6px black)'}}>
{/* <div> */}
  <Link to={slug} className={`PostCard ${className}`}>
    {/* {featuredImage && (
      <div className="PostCard--Image relative">
        <Image background src={featuredImage} alt={title} />
      </div>
    )} */}
    <div className="PostCard--Content">
      {title && <h3 className="PostCard--Title">{title}</h3>}
      <div className="PostCard--Category">
        {categories && categories.map(cat => cat.category).join(', ')}
      </div>
      {excerpt && <div className="PostCard--Excerpt"><p style={{fontSize: '14px'}}>{excerpt}</p></div>}
    </div>
  </Link>
  </div>
  </>
)

export default PostCard
